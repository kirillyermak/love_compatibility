//
//  ZodiacSignManager.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 25.01.2021.
//  Copyright © 2021 Test. All rights reserved.
//

import UIKit
import SwiftDate

//swiftlint:disable all
final class ZodiacSignManager {
    static let shared = ZodiacSignManager()
    
    private init() {}
    
    func signFrom(point: CGFloat, type: WheelType) -> ZodiacSign? {
        if type == .left {
            if (ZodiacSign.aries.leftAngle-15...ZodiacSign.aries.leftAngle+15).contains(point) {
                return .aries
            } else if (ZodiacSign.taurus.leftAngle-15...ZodiacSign.taurus.leftAngle+15).contains(point) {
                return .taurus
            } else if (ZodiacSign.gemini.leftAngle-15...ZodiacSign.gemini.leftAngle+15).contains(point) {
                return .gemini
            } else if (ZodiacSign.cancer.leftAngle-15...ZodiacSign.cancer.leftAngle+15).contains(point) {
                return .cancer
            } else if (ZodiacSign.leo.leftAngle-15...ZodiacSign.leo.leftAngle+15).contains(point) {
                return .leo
            } else if (ZodiacSign.virgo.leftAngle-15...ZodiacSign.virgo.leftAngle+15).contains(point) {
                return .virgo
            } else if (ZodiacSign.libra.leftAngle-15...ZodiacSign.libra.leftAngle+15).contains(point) {
                return .libra
            } else if (ZodiacSign.scorpio.leftAngle-15...ZodiacSign.scorpio.leftAngle+15).contains(point) {
                return .scorpio
            } else if (ZodiacSign.sagittarius.leftAngle-15...ZodiacSign.sagittarius.leftAngle+15).contains(point) {
                return .sagittarius
            } else if (ZodiacSign.capricorn.leftAngle-15...ZodiacSign.capricorn.leftAngle+15).contains(point) {
                return .capricorn
            } else if (ZodiacSign.aquarius.leftAngle-15...ZodiacSign.aquarius.leftAngle+15).contains(point) {
                return .aquarius
            } else if (ZodiacSign.pisces.leftAngle-15...ZodiacSign.pisces.leftAngle+15).contains(point) {
                return .pisces
            }
        } else if type == .right {
            if (ZodiacSign.aries.rightAngle-15...ZodiacSign.aries.rightAngle+15).contains(point) {
                return .aries
            } else if (ZodiacSign.taurus.rightAngle-15...ZodiacSign.taurus.rightAngle+15).contains(point) {
                return .taurus
            } else if (ZodiacSign.gemini.rightAngle-15...ZodiacSign.gemini.rightAngle+15).contains(point) {
                return .gemini
            } else if (ZodiacSign.cancer.rightAngle-15...ZodiacSign.cancer.rightAngle+15).contains(point) {
                return .cancer
            } else if (ZodiacSign.leo.rightAngle-15...ZodiacSign.leo.rightAngle+15).contains(point) {
                return .leo
            } else if (ZodiacSign.virgo.rightAngle-15...ZodiacSign.virgo.rightAngle+15).contains(point) {
                return .virgo
            } else if (ZodiacSign.libra.rightAngle-15...ZodiacSign.libra.rightAngle+15).contains(point) {
                return .libra
            } else if (ZodiacSign.scorpio.rightAngle-15...ZodiacSign.scorpio.rightAngle+15).contains(point) {
                return .scorpio
            } else if (ZodiacSign.sagittarius.rightAngle-15...ZodiacSign.sagittarius.rightAngle+15).contains(point) {
                return .sagittarius
            } else if (ZodiacSign.capricorn.rightAngle-15...ZodiacSign.capricorn.rightAngle+15).contains(point) {
                return .capricorn
            } else if (ZodiacSign.aquarius.rightAngle-15...ZodiacSign.aquarius.rightAngle+15).contains(point) {
                return .aquarius
            } else if (ZodiacSign.pisces.rightAngle-15...ZodiacSign.pisces.rightAngle+15).contains(point) {
                return .pisces
            }
        } else if type == .bottom {
            if (ZodiacSign.aries.bottomAngle-15...ZodiacSign.aries.bottomAngle+15).contains(point) {
                return .aries
            } else if (ZodiacSign.taurus.bottomAngle-15...ZodiacSign.taurus.bottomAngle+15).contains(point) {
                return .taurus
            } else if (ZodiacSign.gemini.bottomAngle-15...ZodiacSign.gemini.bottomAngle+15).contains(point) {
                return .gemini
            } else if (ZodiacSign.cancer.bottomAngle-15...ZodiacSign.cancer.bottomAngle+15).contains(point) {
                return .cancer
            } else if (ZodiacSign.leo.bottomAngle-15...ZodiacSign.leo.bottomAngle+15).contains(point) {
                return .leo
            } else if (ZodiacSign.virgo.bottomAngle-15...ZodiacSign.virgo.bottomAngle+15).contains(point) {
                return .virgo
            } else if (ZodiacSign.libra.bottomAngle-15...ZodiacSign.libra.bottomAngle+15).contains(point) {
                return .libra
            } else if (ZodiacSign.scorpio.bottomAngle-15...ZodiacSign.scorpio.bottomAngle+15).contains(point) {
                return .scorpio
            } else if (ZodiacSign.sagittarius.bottomAngle-15...ZodiacSign.sagittarius.bottomAngle+15).contains(point) {
                return .sagittarius
            } else if (ZodiacSign.capricorn.bottomAngle-15...ZodiacSign.capricorn.bottomAngle+15).contains(point) {
                return .capricorn
            } else if (ZodiacSign.aquarius.bottomAngle-15...ZodiacSign.aquarius.bottomAngle+15).contains(point) {
                return .aquarius
            } else if (ZodiacSign.pisces.bottomAngle-15...ZodiacSign.pisces.bottomAngle+15).contains(point) {
                return .pisces
            }
        }
        
        return nil
    }
    
    func signFrom(_ date: Date) -> ZodiacSign {
        let date = DateInRegion(components: {
            $0.day = date.dateComponents.day
            $0.month = date.dateComponents.month
            $0.year = 2010
        }, region: .current)!
        
        let ariesRange = DateInRegion("2010-03-21", region: .current)!...DateInRegion("2010-04-20", region: .current)!
        let taurusRange = DateInRegion("2010-04-21", region: .current)!...DateInRegion("2010-05-21", region: .current)!
        let geminiRange = DateInRegion("2010-05-22", region: .current)!...DateInRegion("2010-06-21", region: .current)!
        let cancerRange = DateInRegion("2010-06-22", region: .current)!...DateInRegion("2010-07-22", region: .current)!
        let leoRange = DateInRegion("2010-07-23", region: .current)!...DateInRegion("2010-08-21", region: .current)!
        let virgoRange = DateInRegion("2010-08-22", region: .current)!...DateInRegion("2010-09-23", region: .current)!
        let libraRange = DateInRegion("2010-09-24", region: .current)!...DateInRegion("2010-10-23", region: .current)!
        let scorpioRange = DateInRegion("2010-10-24", region: .current)!...DateInRegion("2010-11-22", region: .current)!
        let sagittariusRange = DateInRegion("2010-11-23", region: .current)!...DateInRegion("2010-12-22", region: .current)!
        let aquariusRange = DateInRegion("2010-01-21", region: .current)!...DateInRegion("2010-02-19", region: .current)!
        let piscesRange = DateInRegion("2010-02-20", region: .current)!...DateInRegion("2010-03-20", region: .current)!
        
        var zodiacSign: ZodiacSign
        
        if date.isInRange(date: ariesRange.lowerBound, and: ariesRange.upperBound) {
            zodiacSign = .aries
        } else if date.isInRange(date: taurusRange.lowerBound, and: taurusRange.upperBound) {
            zodiacSign = .taurus
        } else if date.isInRange(date: geminiRange.lowerBound, and: geminiRange.upperBound) {
            zodiacSign = .gemini
        } else if date.isInRange(date: cancerRange.lowerBound, and: cancerRange.upperBound) {
            zodiacSign = .cancer
        } else if date.isInRange(date: leoRange.lowerBound, and: leoRange.upperBound) {
            zodiacSign = .leo
        } else if date.isInRange(date: virgoRange.lowerBound, and: virgoRange.upperBound) {
            zodiacSign = .virgo
        } else if date.isInRange(date: libraRange.lowerBound, and: libraRange.upperBound) {
            zodiacSign = .libra
        } else if date.isInRange(date: scorpioRange.lowerBound, and: scorpioRange.upperBound) {
            zodiacSign = .scorpio
        } else if date.isInRange(date: sagittariusRange.lowerBound, and: sagittariusRange.upperBound) {
            zodiacSign = .sagittarius
        } else if date.isInRange(date: aquariusRange.lowerBound, and: aquariusRange.upperBound) {
            zodiacSign = .aquarius
        } else if date.isInRange(date: piscesRange.lowerBound, and: piscesRange.upperBound) {
            zodiacSign = .pisces
        } else {
            zodiacSign = .capricorn
        }
        
        return zodiacSign
    }
}
