//
//  RewardedAdManager.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 24.01.2021.
//  Copyright © 2021 Test. All rights reserved.
//

import GoogleMobileAds
import SVProgressHUD

protocol RewardedAdHelperDelegate: class {
    func rewardedAdHelperDidRecieveReward()
}

final class RewardedAdHelper: NSObject {
    // MARK: - Computed properties
    var adUnitId: String {
        return isDebug ? "ca-app-pub-3940256099942544/1712485313" : "ca-app-pub-2678601759115686/5886942222"
    }
    
    // MARK: Properties
    weak var delegate: RewardedAdHelperDelegate?
    
    private var ad: GADRewardedAd?
    private var rewarded = false
    
    func createAndShowAd(from viewController: UIViewController) {
        SVProgressHUD.show()
        
        GADRewardedAd.load(withAdUnitID: adUnitId, request: GADRequest()) { [weak self] (ad, error) in
            SVProgressHUD.dismiss()
            
            if let error = error {
                Log.error(.requestRewardedAd, parameters: ["description": error.localizedDescription])
            } else {
                guard let `self` = self, let rewarded = ad else { return }
                
                self.ad = rewarded
                    
                rewarded.fullScreenContentDelegate = self
                
                Log.info(.rewardedAdShown)
                rewarded.present(fromRootViewController: viewController) { [weak self] in
                    self?.rewarded = true
                }
            }
        }
    }
}

// MARK: GADFullScreenContentDelegate
extension RewardedAdHelper: GADFullScreenContentDelegate {
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        Log.error(.showRewardedAd, parameters: ["description": String(describing: error)])
        rewarded = false
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        if rewarded {
            delegate?.rewardedAdHelperDidRecieveReward()
            rewarded = false
        }
    }
}
