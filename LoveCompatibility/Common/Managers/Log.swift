//
//  Log.swift
//  LoveCompatibility
//
//  Created by Kirill on 25.02.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import YandexMobileMetrica

final class Log {
    static func event(name: String, parameters: [String: Any] = [:]) {
        YMMYandexMetrica.reportEvent(name, parameters: parameters, onFailure: nil)
    }
    
    static func error(_ event: Event.Error, parameters: [String: Any] = [:]) {
        debugPrint("💔 ERROR: \(event.rawValue) with parametes \(parameters)")
        Log.event(name: event.rawValue, parameters: parameters)
    }
    
    static func info(_ event: Event.Info, parameters: [String: Any] = [:]) {
        debugPrint("💚 INFO: \(event.rawValue) with parameters \(parameters)")
        Log.event(name: event.rawValue, parameters: parameters)
    }
}

enum Event {
    enum Info: String {
        // Ad
        case openAppAdShown
        case rewardedAdShown
        
        // Purchase
        case purchaseSuccess
        case restoreSuccess
        case retrieveProductsInfoSuccess
        
        // RemoteConfig
        case remoteConfigFetched
    }
    
    enum Error: String {
        case uncased
        
        // Notifications
        case notificationsError
        
        // Remote
        case noInternetConnection
        case requestError
        case decodingError
        
        // Ad
        case requestOpenAppAd
        case showOpenAd
        case requestRewardedAd
        case showRewardedAd
        
        // Purchase
        case purchaseFailure
        case restoreFailure
        case retrieveProductsInfoFailure
        
        // RemoteConfig
        case remoteConfigFetchError
    }
}
