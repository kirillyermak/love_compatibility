//
//  PurchaseManager.swift
//  LoveCompatibility
//
//  Created by Kirill on 19.02.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import SwiftyStoreKit
import SVProgressHUD

struct PurchaseId {
    static let monthlyTrial = "zodiac_monthly_trial"
    static let weekTrial = "zodiac_week_trial"
    static let yearlyTrial = "zodiac_yearly_trial"
}

final class PurchaseManager {
    static let shared = PurchaseManager()
    
    var isPremium: Bool = true {
        didSet(oldValue) {
            if oldValue != self.isPremium {
                // Notify
            }
        }
    }

    var premiumProductsInfo: [ProductInfo]?
    private let reachability = Reachability()
    private let secret = "83494163712049ffad8e4df989b12fad"
    
    private let purchaseIds: Set<String> = [
        PurchaseId.monthlyTrial,
        PurchaseId.weekTrial,
        PurchaseId.yearlyTrial
    ]
    
    private init() {
        completeTransactions()
        verifyReceipts()
        retrieveProductsInfo()
    }
    
    func completeTransactions() {
        SwiftyStoreKit.completeTransactions(atomically: true) { [weak self] purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
//                    self?.isPremium = true
                default:
                    break
                }
            }
        }
    }
    
    func purchaseProduct(id: String, completion: @escaping (Bool) -> Void) {
        SVProgressHUD.show()
        
        SwiftyStoreKit.purchaseProduct(id) { [weak self] (result) in
            SVProgressHUD.dismiss()
            
            switch result {
            case .success(let product):
//                self?.isPremium = true
                
                if product.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(product.transaction)
                }
                
                Log.info(.purchaseSuccess, parameters: ["productId": product.productId])
                completion(true)
            case .error(let error):
                switch error.code {
                case .unknown:
                    Log.error(.purchaseFailure, parameters: ["description": "Unknown error. Please contact support"])
                case .clientInvalid:
                    Log.error(.purchaseFailure, parameters: ["description": "Not allowed to make the payment"])
                case .paymentInvalid:
                    Log.error(.purchaseFailure, parameters: ["description": "The purchase identifier was invalid"])
                case .paymentNotAllowed:
                    Log.error(.purchaseFailure, parameters: ["description": "The device is not allowed to make the payment"])
                case .storeProductNotAvailable:
                    Log.error(.purchaseFailure, parameters: ["description": "The product is not available in the current storefront"])
                case .cloudServicePermissionDenied:
                    Log.error(.purchaseFailure, parameters: ["description": "Access to cloud service information is not allowed"])
                case .cloudServiceNetworkConnectionFailed:
                    Log.error(.purchaseFailure, parameters: ["description": "Could not connect to the network"])
                case .cloudServiceRevoked:
                    Log.error(.purchaseFailure, parameters: ["description": "User has revoked permission to use this cloud service"])
                default:
                    Log.error(.purchaseFailure, parameters: ["description": (error as NSError).description])
                }
                
                completion(false)
            }
        }
    }
    
    func retrieveProductsInfo() {
        SwiftyStoreKit.retrieveProductsInfo(purchaseIds) { [weak self] (result) in
            if result.retrievedProducts.count > 0 {
                self?.premiumProductsInfo = result.retrievedProducts.map { return ProductInfo(product: $0) }
                Log.info(.retrieveProductsInfoSuccess)
            }
            
            if result.invalidProductIDs.count > 0 {
                Log.error(.retrieveProductsInfoFailure, parameters: ["invalidProductIDs": result.invalidProductIDs])
            }
            
            if let error = result.error {
                Log.error(.retrieveProductsInfoFailure, parameters: ["description": error.localizedDescription])
            }
        }
    }
    
    func verifyReceipts() {
        let validator = AppleReceiptValidator(service: isDebug ? .sandbox : .production, sharedSecret: secret)
        SwiftyStoreKit.verifyReceipt(using: validator) { [weak self] (result) in
            var status: PurchaseStatus = .notPurchased
            
            switch result {
            case .success(let receipt):
                verification: for id in self!.purchaseIds {
                    let purchaseResult = SwiftyStoreKit.verifySubscription(ofType: .autoRenewable, productId: id, inReceipt: receipt)
                    
                    switch purchaseResult {
                    case .purchased(let expiryDate, let items):
                        print("PurchasesService: \(id) is valid until \(expiryDate)\n\(items)\n")
                        status = .purchased
                        break verification
                    case .expired(let expiryDate, let items):
                        print("PurchasesService: \(id) is expired since \(expiryDate)\n\(items)\n")
                        status = .expired
                    case .notPurchased:
                        print("PurchasesService: The user has never purchased \(id)")
                        status = .notPurchased
                    }
                }
                
//                self?.isPremium = status == .purchased
            case .error(let error):
//                self?.isPremium = false
                print("PurchasesService: Receipt verification failed: \(error)")
            }
        
        }
    }
    
    enum PurchaseStatus {
        case purchased, notPurchased, expired, unknown
    }
    
    func restore(completion: @escaping (Bool) -> Void) {
        guard Reachability.isReachable() else {
            completion(false)
            return
        }
        
        SVProgressHUD.show()

        SwiftyStoreKit.restorePurchases { [weak self] (results) in
            SVProgressHUD.dismiss()
            
            if results.restoreFailedPurchases.count > 0 {
                Log.error(.restoreFailure, parameters: ["failedPurchases": results.restoreFailedPurchases])
                completion(false)
            } else if results.restoredPurchases.count > 0 {
//                self?.isPremium = true
                Log.info(.restoreSuccess)
                completion(true)
            } else {
                Log.error(.restoreFailure, parameters: ["description": "Nothing to Restore"])
                completion(false)
            }
        }
    }
}
