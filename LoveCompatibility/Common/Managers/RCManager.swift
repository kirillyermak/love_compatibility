//
//  RCManager.swift
//  LoveCompatibility
//
//  Created by Kirill on 03.03.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import Foundation
import FirebaseRemoteConfig

enum RCKey: String, CaseIterable {
    // Purchase
    case purchaseId
}

class RCManager {
    // MARK: - Properties
    static let shared = RCManager()
    
    let remoteConfig: RemoteConfig
    let fetchAttemptLimit = 5

    var fetchFailCouter = 0
    
    // MARK: - Init
    private init() {
        self.remoteConfig = RemoteConfig.remoteConfig()

        let settings = RemoteConfigSettings()
        settings.fetchTimeout = 3
        settings.minimumFetchInterval = isDebug ? 0 : 5 * 60 * 60

        self.remoteConfig.configSettings = settings
        self.remoteConfig.setDefaults(fromPlist: "RCDefaults")
        
        self.fetchCloudValues()
    }
    
    private func fetchCloudValues() {
        self.remoteConfig.fetchAndActivate { [weak self] (status, error) in
            guard let `self` = self else { return }

            switch status {
            case .successFetchedFromRemote:
                Log.info(.remoteConfigFetched)
            default:
                Log.error(.remoteConfigFetchError, parameters: ["description": String(describing: error)])
                
                self.fetchFailCouter += 1

                if self.fetchFailCouter <= self.fetchAttemptLimit {
                    self.fetchCloudValues()
                }
            }
        }
    }
    
    // MARK: - RemoteConfig
    func bool(forKey key: RCKey) -> Bool {
        return self.remoteConfig.configValue(forKey: key.rawValue).boolValue
    }
    
    func string(forKey key: RCKey) -> String? {
        return self.remoteConfig.configValue(forKey: key.rawValue).stringValue
    }
    
    func int(forKey key: RCKey) -> Int? {
        return self.remoteConfig.configValue(forKey: key.rawValue).numberValue.intValue
    }
    
    func float(forKey key: RCKey) -> Float? {
        return self.remoteConfig.configValue(forKey: key.rawValue).numberValue.floatValue
    }
    
    func data(forKey key: RCKey) -> Data {
        return self.remoteConfig.configValue(forKey: key.rawValue).dataValue
    }
}
