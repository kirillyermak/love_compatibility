//
//  ShareManager.swift
//  LoveCompatibility
//
//  Created by Kirill on 02.03.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import UIKit
import Photos

final class ShareManager {
    static let shared = ShareManager()
    private let appUrl = "https://apps.apple.com/ua/app/inspiration-quotes-sayings/id1535617407" // TODO: Change url
    
    private init() {}
    
    func share() -> UIActivityViewController {
        let text =
        """
        \(R.string.localizable.shareFinalText()):
        \(appUrl)
        """
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        activityViewController.activityItemsConfiguration = [
            UIActivity.ActivityType.message
        ] as? UIActivityItemsConfigurationReading
        
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.airDrop
        ]
        
        activityViewController.isModalInPresentation = true
        return activityViewController
    }
    
    func share(pair: ZodiacPair, percent: Int) -> UIActivityViewController {
        let text = getText(pair: pair, percent: percent)
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        activityViewController.activityItemsConfiguration = [
            UIActivity.ActivityType.message
        ] as? UIActivityItemsConfigurationReading
        
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.airDrop
        ]
        
        activityViewController.isModalInPresentation = true
        return activityViewController
    }
    
    private func getText(pair: ZodiacPair, percent: Int) -> String {
        let text =
        """
        \(R.string.localizable.shareI()) \(pair.user.title), \(R.string.localizable.shareMyPartner()) \(pair.partner.title) \(R.string.localizable.shareAndWeCompatibleFor()) \(percent)%

        \(R.string.localizable.shareFinalText()):
        \(appUrl)
        """
        
        return text
    }
}
