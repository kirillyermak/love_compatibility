//
//  CompatibilitiesManager.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 14.01.2021.
//

import Foundation

final class CompatibilitiesManager {
    static let shared = CompatibilitiesManager()
    
    private var fetchedCompatibilities: [Compatibility]?
    
    private init() {
        fetchAll()
    }
    
    @discardableResult
    func fetchAll() -> [Compatibility]? {
        if let fetchedCompatibilities = fetchedCompatibilities {
            return fetchedCompatibilities
        }
        
        guard
            let courseURL = Bundle.main.url(forResource: "love_compatibilities", withExtension: "json"),
            let data = try? String(contentsOf: courseURL, encoding: .utf8).data(using: .utf8)
        else { return nil }
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        do {
            return try decoder.decode([Compatibility].self, from: data)
        } catch {
            Log.error(.decodingError, parameters: ["description": String(describing: error)])
        }
        
        return nil
    }
    
    func fetchFor(female: ZodiacSign, male: ZodiacSign) -> Compatibility? {
        return fetchAll()?.first(where: { $0.id == female.femaleId + "-" + male.maleId})
    }
}
