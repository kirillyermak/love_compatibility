//
//  AppOpenAdManager.swift
//  LoveCompatibility
//
//  Created by Kirill on 03.03.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import GoogleMobileAds

final class AppOpenAdManager: NSObject {
    // MARK: - Computed properties
    var adUnitId: String {
        return isDebug ? "ca-app-pub-3940256099942544/5662855259" : "ca-app-pub-2678601759115686/8224798129"
    }

    // MARK: - Properties
    static let shared = AppOpenAdManager()
    
    private override init() {
        super.init()
        
        if AppPreferences.shared.session >= 3 && PurchaseManager.shared.isPremium {
            requestAppOpenAd()

            Timer.scheduledTimer(withTimeInterval: 200, repeats: true) { [weak self] _ in
                self?.requestAppOpenAd()
            }
        }
    }
    
    func requestAppOpenAd() {
        guard PurchaseManager.shared.isPremium == false else { return }
            
        GADAppOpenAd.load(
            withAdUnitID: adUnitId,
            request: GADRequest(),
            orientation: .portrait
        ) { [weak self] (ad, error) in
            if let error = error {
                Log.error(.requestOpenAppAd, parameters: ["description": String(describing: error)])
            }
            
            if let ad = ad {
                ad.fullScreenContentDelegate = self
                
                let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

                if var topController = keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    
                    ad.present(fromRootViewController: topController)
                }
            }
        }
    }
}

// MARK: - GADFullScreenContentDelegate
extension AppOpenAdManager: GADFullScreenContentDelegate {
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        Log.error(.showOpenAd, parameters: ["description": String(describing: error)])
    }
}
