//
//  PushNotificationsManager.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import UserNotifications

final class PushNotificationsManager: NSObject {
    static let shared = PushNotificationsManager()
    
    private let center = UNUserNotificationCenter.current()
    
    private override init() {
        super.init()
        self.center.delegate = self
    }
    
    func getAuthorizaionStatus(completion: @escaping (Bool) -> Void) {
        self.center.getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .authorized:
                AppPreferences.shared.isNotificationsEnabled = true
                completion(true)
            default:
                AppPreferences.shared.isNotificationsEnabled = false
                completion(false)
            }
        }
    }
    
    func requestAuthorization(completion: ((Bool) -> Void)? = nil) {
        self.getAuthorizaionStatus { [weak self] isNotificationAllowed in
            if isNotificationAllowed == true {
                completion?(true)
                return
            }
            
            let options: UNAuthorizationOptions = [.alert, .sound, .badge]
            self?.center.requestAuthorization(options: options) { (grant, error) in
                if let error = error {
                    Log.error(.notificationsError, parameters: ["description": String(describing: error)])
                    return
                }
                
                AppPreferences.shared.isNotificationsEnabled = grant
                completion?(grant)
            }
        }
    }
    
    func scheduledMorningNotifications() {
        cancelNotifications()
        
        guard let sign = AppPreferences.shared.zodiacSign else { return }
        
        let notificationContent = UNMutableNotificationContent()
        
        let variants = [
            R.string.localizable.pushNotificationText1(),
            R.string.localizable.pushNotificationText2(),
            R.string.localizable.pushNotificationText3(),
            R.string.localizable.pushNotificationText4(),
            R.string.localizable.pushNotificationText5(),
            R.string.localizable.pushNotificationText6()
        ]
    
        notificationContent.title = "\(sign.emoji) \(R.string.localizable.pushNotificationTitle())"
        let index = Int.random(in: 0..<variants.count)
        notificationContent.body = variants[index]
        
        notificationContent.sound = .default

        var datComp = DateComponents()
        datComp.hour = 10
        datComp.minute = 0
        let trigger = UNCalendarNotificationTrigger(dateMatching: datComp, repeats: true)
        let request = UNNotificationRequest(identifier: "ID", content: notificationContent, trigger: trigger)
        
        center.add(request) { (error) in
            if let error = error {
                Log.error(.notificationsError, parameters: ["description": String(describing: error)])
            }
        }
    }
    
    func cancelNotifications() {
        center.removeAllPendingNotificationRequests()
    }
}

// MARK: - UNUserNotificationCenterDelegate
extension PushNotificationsManager: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
}
