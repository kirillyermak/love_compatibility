//
//  HoroscopeManager.swift
//  LoveCompatibility
//
//  Created by Kirill on 28.02.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import Foundation
import SwiftyXMLParser

final class HoroscopeManager: NSObject {
    private let zodiacSigns = [
        ZodiacSign.aries.id,
        ZodiacSign.taurus.id,
        ZodiacSign.gemini.id,
        ZodiacSign.cancer.id,
        ZodiacSign.leo.id,
        ZodiacSign.virgo.id,
        ZodiacSign.libra.id,
        ZodiacSign.scorpio.id,
        ZodiacSign.sagittarius.id,
        ZodiacSign.capricorn.id,
        ZodiacSign.aquarius.id,
        ZodiacSign.pisces.id
    ]
    
    private let horoscopes: [HoroscopeType] = [
        .base,
        .erotic,
        .anti,
        .business,
        .health,
        .cooking,
        .love,
        .mobile
    ]
    
    static let shared = HoroscopeManager()
    
    var baseHoro: [String: Horoscope?] = [:]
    var eroticHoro: [String: Horoscope?] = [:]
    var antiHoro: [String: Horoscope?] = [:]
    var businessHoro: [String: Horoscope?] = [:]
    var healthHoro: [String: Horoscope?] = [:]
    var cookingHoro: [String: Horoscope?] = [:]
    var loveHoro: [String: Horoscope?] = [:]
    var mobileHoro: [String: Horoscope?] = [:]
    
    private let xmlParser = XMLParser()
    
    private override init() {}
    
    func requestHoroscopes() {
        for horo in horoscopes {
            requestHoroscope(horo)
        }
    }
    
    private func requestHoroscope(_ type: HoroscopeType) {
        guard let url = URL(string: "https://ignio.com/r/export/utf/xml/daily/\(type.rawValue).xml") else { return }
        
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.urlCache = nil

        let session = URLSession.init(configuration: config)
        
        session.dataTask(with: url) { [weak self] (data, _, error) in
            if let error = error {
                Log.error(.requestError, parameters: ["sedcriprion": error.localizedDescription])
                return
            }
            
            if let data = data, let zodiacSigns = self?.zodiacSigns {
                let result = XML.parse(data)
                
                for id in zodiacSigns {
                    let horoscope = Horoscope(yesterday: result["horo"][id]["yesterday"].text?.replacingOccurrences(of: "\n", with: ""),
                                              today: result["horo"][id]["today"].text?.replacingOccurrences(of: "\n", with: ""),
                                              tomorrow: result["horo"][id]["tomorrow"].text?.replacingOccurrences(of: "\n", with: ""),
                                              tomorrow02: result["horo"][id]["tomorrow02"].text?.replacingOccurrences(of: "\n", with: ""))
                    
                    switch type {
                    case .base:
                        self?.baseHoro[id] = horoscope
                    case .erotic:
                        self?.eroticHoro[id] = horoscope
                    case .anti:
                        self?.antiHoro[id] = horoscope
                    case .business:
                        self?.businessHoro[id] = horoscope
                    case .health:
                        self?.healthHoro[id] = horoscope
                    case .cooking:
                        self?.cookingHoro[id] = horoscope
                    case .love:
                        self?.loveHoro[id] = horoscope
                    case .mobile:
                        self?.mobileHoro[id] = horoscope
                    }
                }
            }
        }.resume()
    }
}

struct Horoscope {
    let yesterday: String?
    let today: String?
    let tomorrow: String?
    let tomorrow02: String?
}

enum HoroscopeType: String {
    case base = "com"
    case erotic = "ero"
    case anti = "anti"
    case business = "bus"
    case health = "hea"
    case cooking = "cook"
    case love = "lov"
    case mobile = "mob"
    
    func emoji() -> String {
        switch self {
        case .base:
            return "🏠"
        case .erotic:
            return "🔞"
        case .anti:
            return "🙃"
        case .business:
            return "📈"
        case .health:
            return "😷"
        case .cooking:
            return "🍳"
        case .love:
            return "❤️"
        case .mobile:
            return "📱"
        }
    }
    
    func title() -> String {
        switch self {
        case .base:
            return R.string.localizable.horoscopeBase()
        case .erotic:
            return R.string.localizable.horoscopeErotic()
        case .anti:
            return R.string.localizable.horoscopeAnti()
        case .business:
            return R.string.localizable.horoscopeBusiness()
        case .health:
            return R.string.localizable.horoscopeHealth()
        case .cooking:
            return R.string.localizable.horoscopeCooking()
        case .love:
            return R.string.localizable.horoscopeLove()
        case .mobile:
            return R.string.localizable.horoscopeMobile()
        }
    }
}
