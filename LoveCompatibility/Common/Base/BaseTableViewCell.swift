//
//  BaseTableViewCell.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import UIKit

class BaseTableViewCell: UITableViewCell, DefaultReuseIdentifier {
    // MARK: Cell lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        setupLayout()
        setupAppearance()
    }

    // MARK: Functions
    func addSubviews() {}
    func setupLayout() {}
    func setupAppearance() {
        selectionStyle = .none
        backgroundColor = .clear
    }
    func setupActions() {}
    
    // MARK: Configure
    func configure() {
        addSubviews()
        setupActions()
    }
}
