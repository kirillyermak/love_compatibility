//
//  ScaleTransition.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 24.01.2021.
//  Copyright © 2021 Test. All rights reserved.
//

import UIKit
import VisualEffectView

class ScaleTransition: NSObject, UIViewControllerTransitioningDelegate {
    private lazy var animator = ScaleTransitionAnimator()
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.animator.transition = .present
        return self.animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.animator.transition = .dismiss
        return self.animator
    }
}

class ScaleTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    var transition: Transition = .present
    
    private let blurEffectView: VisualEffectView
    
    override init() {
        let visualEffectView = VisualEffectView()
        visualEffectView.colorTint = UIColor.black
        visualEffectView.colorTintAlpha = 0.45
        visualEffectView.blurRadius = 7
        self.blurEffectView = visualEffectView
    }
    
    private let animationDuration: TimeInterval = 0.2
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.animationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let toViewController = transitionContext.viewController(forKey: .to),
            let fromViewController = transitionContext.viewController(forKey: .from),
            let fromView = fromViewController.view,
            let toView = toViewController.view
            else { return assertionFailure("Invalid transitionContext for NoteTransitionAnimator") }
        
        self.blurEffectView.frame = fromView.frame
        
        switch self.transition {
        case .present:
            self.handlePresentTransition(fromView: fromView, toView: toView, context: transitionContext)
        case .dismiss:
            self.handleDismissTransition(fromView: fromView, toView: toView, context: transitionContext)
        }
    }
    
    private func handlePresentTransition(fromView: UIView, toView: UIView, context: UIViewControllerContextTransitioning) {
        toView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        toView.center = fromView.center
        toView.alpha = 0
        
        context.containerView.addSubview(self.blurEffectView)
        context.containerView.addSubview(toView)
        
        UIView.animate(withDuration: self.animationDuration, animations: { [weak self] in
            self?.blurEffectView.alpha = 1
            toView.alpha = 1
            toView.transform = CGAffineTransform(scaleX: 1, y: 1)
        }, completion: { _ in
            context.completeTransition(!context.transitionWasCancelled)
        })
    }
    
    private func handleDismissTransition(fromView: UIView, toView: UIView, context: UIViewControllerContextTransitioning) {
        context.containerView.addSubview(fromView)

        UIView.animate(withDuration: self.animationDuration, animations: { [weak self] in
            self?.blurEffectView.alpha = 0
            fromView.alpha = 0
            fromView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }, completion: { [weak self] _ in
            self?.blurEffectView.removeFromSuperview()
            context.completeTransition(!context.transitionWasCancelled)
        })
    }
    
    enum Transition {
        case present
        case dismiss
    }
}
