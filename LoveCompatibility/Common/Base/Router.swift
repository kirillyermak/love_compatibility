//
//  Router.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 14.01.2021.
//

import UIKit

protocol Routing {
    associatedtype T
    var viewController: T? { get }
}

class Router<T: UIViewController>: Routing {
    weak var viewController: T?
    
    init(_ viewController: T) {
        self.viewController = viewController
    }
}
