//
//  BaseViewController.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 14.01.2021.
//

import UIKit

class BaseViewController: UIViewController {
    // MARK: View lifecyce
    override func loadView() {
        super.loadView()
        addSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupLayout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        setupAppearance()
        setupActions()
    }
    
    // MARK: Functions
    func addSubviews() {}
    func setupLayout() {}
    func setupAppearance() {}
    func setupActions() {}
}
