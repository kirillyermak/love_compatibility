//
//  BaseViewDelegate.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 14.01.2021.
//

import UIKit

protocol BaseViewDelegate: class {
    func viewDidLoad()
    func viewWillAppear()
    func viewDidAppear()
    func viewWillDisappear()
    func viewDidDisappear()
}

extension BaseViewDelegate {
    func viewDidLoad() {}
    func viewWillAppear() {}
    func viewDidAppear() {}
    func viewWillDisappear() {}
    func viewDidDisappear() {}
}
