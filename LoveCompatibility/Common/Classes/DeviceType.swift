//
//  DeviceType.swift
//  LoveCompatibility
//
//  Created by Kirill on 02.03.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import UIKit

struct ScreenSize {
    static let width = UIScreen.main.bounds.size.width
    static let height = UIScreen.main.bounds.size.height
    static let frame = CGRect(x: 0, y: 0, width: ScreenSize.width, height: ScreenSize.height)
    static let maxWH = max(ScreenSize.width, ScreenSize.height)
}

enum DeviceType {
    case iPad, iPhone4orLess, iPhone5orSE, iPhone678, iPhone678p, iPhoneX, iPhoneXRorXM, iPhoneXSeries, iPhone12, iPhone12ProMax, unknown
    
    private static var isIphone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    private static var isIpad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad || UIDevice.current.model.hasPrefix("iPad")
    }
    
    static var isX: Bool {
        return isiPhoneX || isiPhoneXRorXM || isiPhone12 || isiPhone12ProMax
    }
    
    static var isBigScreen: Bool {
        return isX || isiPhone678p
    }
    
    static var current: DeviceType {
        if isIpad {
            return .iPad
        }

        if isiPhone4orLess { return .iPhone4orLess }
        if isiPhone5orSE { return .iPhone5orSE }
        if isiPhone678 { return .iPhone678 }
        if isiPhone678p { return .iPhone678p }
        if isiPhoneX { return .iPhoneX }
        if isiPhoneXRorXM { return .iPhoneXRorXM }
        if isiPhone12 { return .iPhone12 }
        if isiPhone12ProMax { return .iPhone12ProMax }

        return .unknown
    }
    
    private static let isiPhone4orLess  = isIphone && ScreenSize.maxWH < 568.0
    static let isiPhone5orSE    = isIphone && ScreenSize.maxWH == 568.0
    private static let isiPhone678      = isIphone && ScreenSize.maxWH == 667.0
    private static let isiPhone678p     = isIphone && ScreenSize.maxWH == 736.0
    private static let isiPhoneX        = isIphone && ScreenSize.maxWH == 812.0
    private static let isiPhoneXRorXM   = isIphone && ScreenSize.maxWH == 896.0
    private static let isiPhone12       = isIphone && ScreenSize.maxWH == 844.0
    private static let isiPhone12ProMax = isIphone && ScreenSize.maxWH == 926.0
}
