//
//  AppPreferences.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import Foundation

final class AppPreferences {
    static let shared = AppPreferences()
    
    private init() {}
    
    let ud = UserDefaults.standard
    
    private var currentSessionData = [PreferenceKey.RawValue: Any]()
}

// MARK: - Keys
extension AppPreferences {
    var gender: Gender? {
        get {
            let int = ud.integer(forKey: PreferenceKey.gender.rawValue)
            return Gender(rawValue: int)
        }
        set {
            ud.set(newValue?.rawValue, forKey: PreferenceKey.gender.rawValue)
        }
    }
    
    var birthDate: Date? {
        get {
            return ud.object(forKey: PreferenceKey.birthDate.rawValue) as? Date
        }
        set {
            ud.set(newValue, forKey: PreferenceKey.birthDate.rawValue)
        }
    }
    
    var zodiacSign: ZodiacSign? {
        get {
            let int = ud.integer(forKey: PreferenceKey.zodiacSign.rawValue)
            return ZodiacSign(rawValue: int)
        }
        set {
            ud.setValue(newValue?.rawValue, forKey: PreferenceKey.zodiacSign.rawValue)
        }
    }
    
    var isOnboardingViewed: Bool {
        get {
            return ud.bool(forKey: PreferenceKey.isOnboardingViewed.rawValue)
        }
        set {
            ud.set(newValue, forKey: PreferenceKey.isOnboardingViewed.rawValue)
        }
    }
    
    var isDarkModeEnabled: Bool {
        get {
            return ud.bool(forKey: PreferenceKey.isDarkModeEnabled.rawValue)
        }
        set {
            ud.set(newValue, forKey: PreferenceKey.isDarkModeEnabled.rawValue)
        }
    }
    
    var isNotificationsEnabled: Bool {
        get {
            return ud.bool(forKey: PreferenceKey.isNotificationsEnabled.rawValue)
        }
        set {
            ud.set(newValue, forKey: PreferenceKey.isNotificationsEnabled.rawValue)
        }
    }
    
    var session: Int {
        get {
            return ud.integer(forKey: PreferenceKey.session.rawValue)
        }
        set {
            ud.setValue(newValue, forKey: PreferenceKey.session.rawValue)
        }
    }
}

// MARK: - Enumerations
extension AppPreferences {
    enum PreferenceKey: String {
        case session
        case gender
        case birthDate
        case zodiacSign
        case isOnboardingViewed
        case isDarkModeEnabled
        case isNotificationsEnabled
    }
}
