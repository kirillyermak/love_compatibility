//
//  DefaultReuseIdentifier.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import Foundation

protocol DefaultReuseIdentifier {
    static var identifier: String { get }
}

extension DefaultReuseIdentifier {
    static var identifier: String {
        return String(describing: self)
    }
}
