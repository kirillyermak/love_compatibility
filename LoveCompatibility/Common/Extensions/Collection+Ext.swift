//
//  Collection+Ext.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

extension Collection {
    func objectAt(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
