//
//  UIViewController+Ext.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import UIKit

extension UIViewController {
    func embedInNavigationController(navigationBarIsHidden isHidden: Bool) -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: self)
        navigationController.navigationBar.isHidden = isHidden
        return navigationController
    }
}
