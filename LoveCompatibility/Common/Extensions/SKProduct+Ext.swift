//
//  SKProduct+Ext.swift
//  LoveCompatibility
//
//  Created by Kirill on 27.02.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import StoreKit

extension SKProduct.PeriodUnit {
    func toPeriod() -> PeriodUnit {
        switch self {
        case .day: return .day
        case .week: return .week
        case .month: return .month
        case .year: return .year
        @unknown default: return .unknown
        }
    }
}

enum PeriodUnit: String {
    case day, week, month, year
    case unknown
}
