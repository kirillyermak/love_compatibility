//
//  UIView+Ext.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import UIKit

extension UIView {
    func roundCorners(topLeft: CGFloat = 0, topRight: CGFloat = 0, bottomLeft: CGFloat = 0, bottomRight: CGFloat = 0) {
        let topLeftRadius = CGSize(width: topLeft, height: topLeft)
        let topRightRadius = CGSize(width: topRight, height: topRight)
        let bottomLeftRadius = CGSize(width: bottomLeft, height: bottomLeft)
        let bottomRightRadius = CGSize(width: bottomRight, height: bottomRight)
        let maskPath = UIBezierPath(shouldRoundRect: self.bounds,
                                    topLeftRadius: topLeftRadius,
                                    topRightRadius: topRightRadius,
                                    bottomLeftRadius: bottomLeftRadius,
                                    bottomRightRadius: bottomRightRadius)
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
    
    func roundCorners(radius: CGFloat) {
        self.roundCorners(topLeft: radius, topRight: radius, bottomLeft: radius, bottomRight: radius)
    }
}
