//
//  Animation+Ext.swift
//  LoveCompatibility
//
//  Created by Kirill on 03.03.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import Lottie

extension Animation {
    static var stars: Animation? {
        return Animation.named("stars")
    }
    
    static var welcome: Animation? {
        return Animation.named("welcome")
    }

    static var result: Animation? {
        return Animation.named("result")
    }
    
    static var noInternet: Animation? {
        return Animation.named("no_internet")
    }
    
    static var coralButton: Animation? {
        return Animation.named("big_btn_coral")
    }
    
    static var greenButton: Animation? {
        return Animation.named("big_btn_green")
    }
    
    static var purpleButton: Animation? {
        return Animation.named("big_btn_purple")
    }
    
    static var redButton: Animation? {
        return Animation.named("big_btn_red")
    }
    
    static var notifications: Animation? {
        return Animation.named("notifications")
    }
}
