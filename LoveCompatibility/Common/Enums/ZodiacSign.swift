//
//  ZodiacSign.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import UIKit

struct ZodiacPair {
    let user: ZodiacSign
    let partner: ZodiacSign
}

enum ZodiacSign: Int {
    case aries
    case taurus
    case gemini
    case cancer
    case leo
    case virgo
    case libra
    case scorpio
    case sagittarius
    case capricorn
    case aquarius
    case pisces
    
    var title: String {
        switch self {
        case .aries:
            return R.string.localizable.zodiacSignAries()
        case .taurus:
            return R.string.localizable.zodiacSignTaurus()
        case .gemini:
            return R.string.localizable.zodiacSignGemini()
        case .cancer:
            return R.string.localizable.zodiacSignCancer()
        case .leo:
            return R.string.localizable.zodiacSignLeo()
        case .virgo:
            return R.string.localizable.zodiacSignVirgo()
        case .libra:
            return R.string.localizable.zodiacSignLibra()
        case .scorpio:
            return R.string.localizable.zodiacSignScorpio()
        case .sagittarius:
            return R.string.localizable.zodiacSignSagittarius()
        case .capricorn:
            return R.string.localizable.zodiacSignCapricorn()
        case .aquarius:
            return R.string.localizable.zodiacSignAquarius()
        case .pisces:
            return R.string.localizable.zodiacSignPisces()
        }
    }
    
    var emoji: String {
        switch self {
        case .aries:
            return "🐏"
        case .taurus:
            return "🐂"
        case .gemini:
            return "👯"
        case .cancer:
            return "🦞"
        case .leo:
            return "🦁"
        case .virgo:
            return "👩‍🦱"
        case .libra:
            return "⚖️"
        case .scorpio:
            return "🦂"
        case .sagittarius:
            return "🏹"
        case .capricorn:
            return "🐐"
        case .aquarius:
            return "🏺"
        case .pisces:
            return "🎏"
        }
    }
    
    var femaleId: String {
        return "f_" + id
    }
    
    var maleId: String {
        return "m_" + id
    }
    
    var bottomAngle: CGFloat {
        switch self {
        case .aries:
            return -90
        case .taurus:
            return -120
        case .gemini:
            return -150
        case .cancer:
            return -180
        case .leo:
            return 150
        case .virgo:
            return 120
        case .libra:
            return 90
        case .scorpio:
            return 60
        case .sagittarius:
            return 30
        case .capricorn:
            return 0
        case .aquarius:
            return -30
        case .pisces:
            return -60
        }
    }
    
    var leftAngle: CGFloat {
        switch self {
        case .aries:
            return 0
        case .taurus:
            return -30
        case .gemini:
            return -60
        case .cancer:
            return -90
        case .leo:
            return -120
        case .virgo:
            return -150
        case .libra:
            return -180
        case .scorpio:
            return 150
        case .sagittarius:
            return 120
        case .capricorn:
            return 90
        case .aquarius:
            return 60
        case .pisces:
            return 30
        }
    }
    
    var rightAngle: CGFloat {
        switch self {
        case .aries:
            return 180
        case .taurus:
            return 150
        case .gemini:
            return 120
        case .cancer:
            return 90
        case .leo:
            return 60
        case .virgo:
            return 30
        case .libra:
            return 0
        case .scorpio:
            return -30
        case .sagittarius:
            return -60
        case .capricorn:
            return -90
        case .aquarius:
            return -120
        case .pisces:
            return -150
        }
    }
    
    var id: String {
        switch self {
        case .aries:
            return "aries"
        case .taurus:
            return "taurus"
        case .gemini:
            return "gemini"
        case .cancer:
            return "cancer"
        case .leo:
            return "leo"
        case .virgo:
            return "virgo"
        case .libra:
            return "libra"
        case .scorpio:
            return "scorpio"
        case .sagittarius:
            return "sagittarius"
        case .capricorn:
            return "capricorn"
        case .aquarius:
            return "aquarius"
        case .pisces:
            return "pisces"
        }
    }
}
