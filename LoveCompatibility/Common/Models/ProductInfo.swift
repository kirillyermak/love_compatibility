//
//  ProductInfo.swift
//  LoveCompatibility
//
//  Created by Kirill on 27.02.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import Foundation
import StoreKit

struct ProductInfo {
    let productId: String
    let title: String
    let price: String?
    let priceValue: NSDecimalNumber
    let currencyCode: String?
    let priceLocale: Locale
    let trial: (duration: Int, period: PeriodUnit)?
    let period: (duration: Int, period: PeriodUnit)?
    
    static func formatPrice(_ price: Float, locale: Locale?) -> String {
        let formatter = NumberFormatter()
        formatter.locale = locale ?? Locale.current
        formatter.numberStyle = .currency
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        return formatter.string(from: NSNumber(value: price)) ?? "$\(price)"
    }
    
    init(product: SKProduct) {
        let formatter = NumberFormatter()
        formatter.locale = product.priceLocale
        formatter.numberStyle = .currency
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2

        self.productId = product.productIdentifier
        self.title = product.localizedTitle
        self.price = formatter.string(from: product.price)
        self.priceValue = product.price
        self.currencyCode = product.priceLocale.currencyCode
        self.priceLocale = product.priceLocale
        let subscPeriod = product.introductoryPrice?.subscriptionPeriod
        self.trial = subscPeriod.map { ($0.numberOfUnits, $0.unit.toPeriod()) }
        self.period = product.subscriptionPeriod.map { ($0.numberOfUnits, $0.unit.toPeriod()) }
    }
}
