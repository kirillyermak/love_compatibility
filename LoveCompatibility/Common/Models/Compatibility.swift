//
//  Compatibility.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 14.01.2021.
//

struct Compatibility: Codable {
    let id: String
    let compatibility: Int
    let description: String
    let love: String
    let sex: String
    let family: String
    let friendship: String
    let work: String
}
