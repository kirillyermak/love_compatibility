//
//  WheelType.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 25.01.2021.
//  Copyright © 2021 Test. All rights reserved.
//

enum WheelType {
    case left
    case right
    case bottom
    case top
}
