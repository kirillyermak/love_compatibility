//
//  PreResultConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 24.01.2021.
//  Copyright (c) 2021 Test. All rights reserved.
//

import UIKit

final class PreResultConfigurator {
    static func configure(delegate: PreResultDelegate) -> UIViewController {
        let viewController = PreResultViewController()
        let router = PreResultRouter(viewController)
        let presenter = PreResultPresenter(view: viewController,
                                           router: router,
                                           delegate: delegate)
        viewController.dataSource = presenter
        viewController.input = presenter
        
        return viewController
    }
}
