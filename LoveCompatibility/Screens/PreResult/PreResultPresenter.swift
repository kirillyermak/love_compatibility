//
//  PreResultPresenter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 24.01.2021.
//  Copyright (c) 2021 Test. All rights reserved.
//

protocol PreResultDelegate: class {
    func watchAdButtonDidTap()
    func activateProButtonDidTap()
}

protocol PreResultView: class {}

final class PreResultPresenter {
    private unowned let view: PreResultView
    private let router: PreResultRouter
    private weak var delegate: PreResultDelegate?
    
    init(view: PreResultView, router: PreResultRouter, delegate: PreResultDelegate) {
        self.view = view
        self.router = router
        self.delegate = delegate
    }
}

// MARK: PreResultDataSource
extension PreResultPresenter: PreResultDataSource {}

// MARK: PreResultInputDelegate
extension PreResultPresenter: PreResultInputDelegate {
    func watchAdButtonDidtap() {
        router.toBack { [weak self] in
            self?.delegate?.watchAdButtonDidTap()
        }
    }
    
    func activateProButtonDidTap() {
        router.toBack { [weak self] in
            self?.delegate?.activateProButtonDidTap()
        }
    }
    
    func backButtonDidTap() {
        router.toBack()
    }
}

// MARK: BaseViewDelegate
extension PreResultPresenter: BaseViewDelegate {}

// MARK: Private functions
private extension PreResultPresenter {}
