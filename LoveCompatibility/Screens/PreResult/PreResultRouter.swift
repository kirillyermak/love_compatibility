//
//  PreResultRouter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 24.01.2021.
//  Copyright (c) 2021 Test. All rights reserved.
//

import UIKit

final class PreResultRouter: Router<PreResultViewController> {
    func toBack(completion: (() -> Void)? = nil) {
        viewController?.dismiss(animated: true, completion: completion)
    }
}
