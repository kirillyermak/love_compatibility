//
//  PreResultViewController.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 24.01.2021.
//  Copyright (c) 2021 Test. All rights reserved.
//

import UIKit
import PinLayout
import Lottie

protocol PreResultDataSource: class {}

protocol PreResultInputDelegate: BaseViewDelegate {
    func watchAdButtonDidtap()
    func activateProButtonDidTap()
    func backButtonDidTap()
}

final class PreResultViewController: BaseViewController {
    // MARK: Properties
    var dataSource: PreResultDataSource?
    var input: PreResultInputDelegate?
    
    // MARK: UI
    private let backButton = UIButton()
    private let containerView = UIView()
    private let animationView = AnimationView(name: "result")
    private let resultLabel = UILabel()
    private let watchAdButton = UIButton()
    private let activateProButton = UIButton()
    
    // MARK: Setup
    override func addSubviews() {
        view.addSubview(backButton)
        containerView.addSubview(animationView)
        containerView.addSubview(resultLabel)
        containerView.addSubview(watchAdButton)
        containerView.addSubview(activateProButton)
        view.addSubview(containerView)
    }
    
    override func setupLayout() {
        backButton.pin
            .all()
        
        containerView.pin
            .height(250)
            .horizontally(30)
            .vCenter()
        
        activateProButton.pin
            .bottom(15)
            .right(10)
            .height(64)
            .width(45%)
        
        watchAdButton.pin
            .left(10)
            .size(of: activateProButton)
            .bottom(15)
        
        resultLabel.pin
            .above(of: watchAdButton)
            .marginBottom(5)
            .horizontally(20)
            .sizeToFit(.width)
        
        animationView.pin
            .above(of: resultLabel)
            .top()
            .horizontally()
    }
    
    override func setupAppearance() {
        containerView.backgroundColor = R.color.background()
        containerView.layer.cornerRadius = 13
        
        resultLabel.text = R.string.localizable.preResultIsReady()
        resultLabel.font = R.font.sfProDisplayBold(size: 30)
        resultLabel.textAlignment = .center
        resultLabel.textColor = R.color.main_text()
        
        watchAdButton.setTitle("\(R.string.localizable.preResultWatchAd())  👎", for: .normal)
        watchAdButton.backgroundColor = R.color.button_purple()?.withAlphaComponent(0.8)
        watchAdButton.setTitleColor(R.color.light_main_text(), for: .normal)
        watchAdButton.titleLabel?.font = R.font.sfProDisplayRegular(size: 20)
        watchAdButton.layer.cornerRadius = 13
        watchAdButton.titleLabel?.lineBreakMode = .byWordWrapping
        
        let gender = AppPreferences.shared.gender
        let activateProButtonTitle = "\(R.string.localizable.sellingBecome().uppercased())  \(gender == .female ? "👸🏽" : "🤴🏽")"
        activateProButton.setTitle(activateProButtonTitle, for: .normal)
        activateProButton.backgroundColor = R.color.button_green()
        activateProButton.setTitleColor(R.color.dark_main_text(), for: .normal)
        activateProButton.titleLabel?.font = R.font.sfProDisplayBold(size: 20)
        activateProButton.layer.cornerRadius = 13
        
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .playOnce
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.play()
    }
    
    override func setupActions() {
        watchAdButton.addTarget(self, action: #selector(watchAdButtonAction), for: .touchUpInside)
        activateProButton.addTarget(self, action: #selector(activateProButtonAction), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
    }
}

// MARK: Button Actions
private extension PreResultViewController {
    @objc private func watchAdButtonAction() {
        input?.watchAdButtonDidtap()
    }
    
    @objc private func activateProButtonAction() {
        input?.activateProButtonDidTap()
    }
    
    @objc private func backButtonAction() {
        input?.backButtonDidTap()
    }
}

// MARK: PreResultView
extension PreResultViewController: PreResultView {}

// MARK: Private functions
private extension PreResultViewController {}
