//
//  NotificationsRouter.swift
//  LoveCompatibility
//
//  Created by Kirill on 04.03.2021.
//  Copyright (c) 2021 kyrylyermak. All rights reserved.
//

import UIKit

final class NotificationsRouter: Router<NotificationsViewController> {
    func toMainScreen() {
        let controller = MainConfigurator.configure()
        viewController?.navigationController?.setViewControllers([controller], animated: true)
    }
}
