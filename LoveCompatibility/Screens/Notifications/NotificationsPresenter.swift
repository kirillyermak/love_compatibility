//
//  NotificationsPresenter.swift
//  LoveCompatibility
//
//  Created by Kirill on 04.03.2021.
//  Copyright (c) 2021 kyrylyermak. All rights reserved.
//

import Haptico

protocol NotificationsView: class {}

final class NotificationsPresenter {
    private unowned let view: NotificationsView
    private let router: NotificationsRouter
    
    init(view: NotificationsView, router: NotificationsRouter) {
        self.view = view
        self.router = router
    }
}

// MARK: NotificationsDataSource
extension NotificationsPresenter: NotificationsDataSource {}

// MARK: NotificationsInputDelegate
extension NotificationsPresenter: NotificationsInputDelegate {
    func nextButtonDidTap() {
        PushNotificationsManager.shared.requestAuthorization { [weak self] (isEnabled) in
            if isEnabled {
                PushNotificationsManager.shared.scheduledMorningNotifications()
            } else {
                Haptico.shared().generate(.error)
            }
            
            DispatchQueue.main.async {
                self?.router.toMainScreen()
            }
        }
    }
}

// MARK: BaseViewDelegate
extension NotificationsPresenter: BaseViewDelegate {}
