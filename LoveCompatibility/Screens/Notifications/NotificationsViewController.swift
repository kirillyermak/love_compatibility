//
//  NotificationsViewController.swift
//  LoveCompatibility
//
//  Created by Kirill on 04.03.2021.
//  Copyright (c) 2021 kyrylyermak. All rights reserved.
//

import UIKit
import PinLayout
import Lottie

protocol NotificationsDataSource: class {}

protocol NotificationsInputDelegate: BaseViewDelegate {
    func nextButtonDidTap()
}

final class NotificationsViewController: BaseViewController {
    // MARK: Properties
    var dataSource: NotificationsDataSource?
    var input: NotificationsInputDelegate?
    
    // MARK: UI
    private let notificationAnimationView = AnimationView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let nextButton = UIButton()
    private let nextButtonAnimationView = AnimationView()
    
    // MARK: Setup
    override func addSubviews() {
        view.addSubview(notificationAnimationView)
        view.addSubview(titleLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(nextButtonAnimationView)
        view.addSubview(nextButton)
    }
    
    override func setupLayout() {
        notificationAnimationView.pin
            .top(5%)
            .horizontally()
            .height(40%)
        
        titleLabel.pin
            .below(of: notificationAnimationView)
            .marginTop(5%)
            .horizontally(20)
            .sizeToFit(.width)
        
        nextButton.pin
            .horizontally(20)
            .bottom(30 + view.pin.safeArea.bottom)
            .height(47)
        
        nextButtonAnimationView.pin
            .center(to: nextButton.anchor.center)
            .height(nextButton.frame.height * 3)
            .width(nextButton.frame.width * 2)
        
        descriptionLabel.pin
            .verticallyBetween(titleLabel, and: nextButtonAnimationView)
            .horizontally(20)
    }
    
    override func setupAppearance() {
        view.backgroundColor = R.color.background()
        
        titleLabel.text = R.string.localizable.notificationsTitle()
        titleLabel.font = R.font.sfProDisplayBold(size: 40)
        titleLabel.textAlignment = .center
        
        descriptionLabel.font = R.font.sfProDisplayMedium(size: 25)
        descriptionLabel.text = R.string.localizable.notificationsSubtitle()
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        
        notificationAnimationView.animation = Animation.notifications
        notificationAnimationView.contentMode = .scaleAspectFit
        notificationAnimationView.loopMode = .loop
        notificationAnimationView.backgroundBehavior = .pauseAndRestore
        notificationAnimationView.play()
        
        nextButton.setTitleColor(R.color.dark_main_text(), for: .normal)
        nextButton.setTitle(R.string.localizable.commonNext(), for: .normal)
        nextButton.titleLabel?.font = R.font.sfProDisplayBold(size: 20)
        
        nextButtonAnimationView.animation = Animation.greenButton
        nextButtonAnimationView.contentMode = .scaleAspectFit
        nextButtonAnimationView.loopMode = .loop
        nextButtonAnimationView.backgroundBehavior = .pauseAndRestore
        nextButtonAnimationView.play()
    }
    
    override func setupActions() {
        nextButton.addTarget(self, action: #selector(nextButtonAction), for: .touchUpInside)
    }
}

// MARK: Button Actions
private extension NotificationsViewController {
    @objc private func nextButtonAction() {
        input?.nextButtonDidTap()
    }
}

// MARK: NotificationsView
extension NotificationsViewController: NotificationsView {}
