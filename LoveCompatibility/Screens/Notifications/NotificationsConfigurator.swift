//
//  NotificationsConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill on 04.03.2021.
//  Copyright (c) 2021 kyrylyermak. All rights reserved.
//

import UIKit

final class NotificationsConfigurator {
    static func configure() -> UIViewController {
        let viewController = NotificationsViewController()
        let router = NotificationsRouter(viewController)
        let presenter = NotificationsPresenter(view: viewController,
                                               router: router)
        viewController.dataSource = presenter
        viewController.input = presenter
        
        return viewController
    }
}
