//
//  GenderPresenter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

enum Gender: Int {
    case female = 1
    case male = 2
}

protocol GenderView: class {
    func updateNextButtonWith(title: String)
}

final class GenderPresenter {
    private unowned let view: GenderView
    private let router: GenderRouter
    private let isOnboarding: Bool
    private weak var delegate: SettingsDelegate?
    private var selectedGendor: Gender?
    
    init(view: GenderView, router: GenderRouter, isOnboarding: Bool, delegate: SettingsDelegate?) {
        self.view = view
        self.router = router
        self.isOnboarding = isOnboarding
        self.delegate = delegate
    }
}

// MARK: GenderDataSource
extension GenderPresenter: GenderDataSource {}

// MARK: GenderInputDelegate
extension GenderPresenter: GenderInputDelegate {
    func genderSelected(_ gender: Gender) {
        selectedGendor = gender
    }
    
    func nextButtonDidTap() {
        AppPreferences.shared.gender = selectedGendor
        
        if isOnboarding {
            AppPreferences.shared.isOnboardingViewed = true
            router.toAgeScreen()
        } else {
            delegate?.update()
            router.toBack()
        }
    }
}

// MARK: BaseViewDelegate
extension GenderPresenter: BaseViewDelegate {
    func viewDidLoad() {
        view.updateNextButtonWith(title: isOnboarding ? R.string.localizable.commonNext() : R.string.localizable.commonSave())
    }
}
