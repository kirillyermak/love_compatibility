//
//  GenderViewController.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import PinLayout
import Lottie
import Haptico

protocol GenderDataSource: class {}

protocol GenderInputDelegate: BaseViewDelegate {
    func genderSelected(_ gender: Gender)
    func nextButtonDidTap()
}

final class GenderViewController: BaseViewController {
    // MARK: Properties
    var dataSource: GenderDataSource?
    var input: GenderInputDelegate?
    
    // MARK: UI
    private let chooseGenderLabel = UILabel()
    private let femaleView = UIView()
    private let femaleLabel = UILabel()
    private let femaleButton = UIButton()
    private let maleView = UIView()
    private let maleLabel = UILabel()
    private let maleButton = UIButton()
    private let nextButton = UIButton()
    private let animationView = AnimationView()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        input?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let gender = AppPreferences.shared.gender {
            setGenderActive(gender)
        } else {
            chooseGenderLabel.text = R.string.localizable.genderChoose()
        }
    }
    
    // MARK: Setup
    override func addSubviews() {
        view.addSubview(femaleView)
        view.addSubview(femaleLabel)
        view.addSubview(femaleButton)
        view.addSubview(maleView)
        view.addSubview(maleLabel)
        view.addSubview(maleButton)
        view.addSubview(animationView)
        view.addSubview(chooseGenderLabel)
        view.addSubview(nextButton)
    }
    
    override func setupLayout() {
        chooseGenderLabel.pin
            .top(view.pin.safeArea.top + 10)
            .horizontally()
            .sizeToFit(.width)
        
        femaleView.pin
            .vertically()
            .width(100%)
            .left(-55%)
        
        femaleView.layer.cornerRadius = femaleView.frame.height / 2
        
        femaleLabel.pin
            .left(50)
            .vertically()
            .width(50%)
        
        femaleButton.pin
            .left()
            .vertically()
            .width(40%)
        
        maleView.pin
            .vertically()
            .width(100%)
            .right(-55%)
        
        maleView.layer.cornerRadius = maleView.frame.height / 2
        
        maleLabel.pin
            .right(50)
            .vertically()
            .width(50%)
        
        maleButton.pin
            .right()
            .vertically()
            .width(40%)
        
        nextButton.pin
            .horizontally(30)
            .bottom(30 + view.pin.safeArea.bottom)
            .height(47)
        
        animationView.pin
            .center(to: nextButton.anchor.center)
            .height(nextButton.frame.height * 3)
            .width(nextButton.frame.width * 2)
    }
    
    override func setupAppearance() {
        view.backgroundColor = R.color.background()
        
        chooseGenderLabel.numberOfLines = 0
        chooseGenderLabel.textAlignment = .center
        chooseGenderLabel.textColor = R.color.main_text()
        chooseGenderLabel.font = R.font.sfProDisplayRegular(size: 34)
        
        let font = R.font.sfProDisplayBold(size: 60)
        
        maleLabel.text = "👱‍♂️"
        maleLabel.font = font
        maleLabel.textAlignment = .right
        
        femaleLabel.text = "👩"
        femaleLabel.font = font
        femaleLabel.textAlignment = .left
        
        maleView.backgroundColor = R.color.secondary_background_light()
        femaleView.backgroundColor = R.color.secondary_background_light()
        
        nextButton.isHidden = true
        nextButton.titleLabel?.font = R.font.sfProDisplayBold(size: 20)
        
        let animation = Animation.purpleButton
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.play()
        animationView.isHidden = true
    }
    
    override func setupActions() {
        femaleButton.addTarget(self, action: #selector(femaleButtonAction), for: .touchUpInside)
        maleButton.addTarget(self, action: #selector(maleButtonAction), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextButtonAction), for: .touchUpInside)
    }
}

// MARK: Button Actions
private extension GenderViewController {
    @objc private func femaleButtonAction() {
        input?.genderSelected(.female)
        setGenderActive(.female)
    }
    
    @objc private func maleButtonAction() {
        input?.genderSelected(.male)
        setGenderActive(.male)
    }
    
    @objc private func nextButtonAction() {
        input?.nextButtonDidTap()
        Haptico.shared().generate(.success)
    }
}

// MARK: GenderView
extension GenderViewController: GenderView {
    func updateNextButtonWith(title: String) {
        nextButton.setTitle(title, for: .normal)
    }
}

// MARK: Private functions
private extension GenderViewController {
    private func setGenderActive(_ gender: Gender) {
        let selectedColor = R.color.button_green()
        let secondaryColor = R.color.secondary_background_light()
        let selectedFont = R.font.sfProDisplayBold(size: 100)
        let secondaryFont = R.font.sfProDisplayBold(size: 60)
        
        let animation = CAKeyframeAnimation(keyPath: "transform.scale")
        animation.values = [1.0, 1.1, 1.0]
        animation.duration = 1.5
        animation.repeatCount = Float.infinity
        
        if gender == .female {
            femaleLabel.text = "👸"
            maleLabel.text = "👨"
            femaleLabel.font = selectedFont
            maleLabel.font = secondaryFont
            femaleView.backgroundColor = selectedColor
            maleView.backgroundColor = secondaryColor
            femaleView.layer.add(animation, forKey: nil)
            maleView.layer.removeAllAnimations()
            chooseGenderLabel.text = R.string.localizable.genderFemale()
        } else {
            femaleLabel.text = "👩"
            maleLabel.text = "🤴"
            femaleLabel.font = secondaryFont
            maleLabel.font = selectedFont
            maleView.backgroundColor = selectedColor
            femaleView.backgroundColor = secondaryColor
            maleView.layer.add(animation, forKey: nil)
            femaleView.layer.removeAllAnimations()
            chooseGenderLabel.text = R.string.localizable.genderMale()
        }
        
        nextButton.isHidden = false
        animationView.isHidden = false
        
        Haptico.shared().generate(.light)
    }
}
