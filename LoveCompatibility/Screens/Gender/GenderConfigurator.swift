//
//  GenderConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class GenderConfigurator {
    static func configure(isOnboarding: Bool = true, delegate: SettingsDelegate? = nil) -> UIViewController {
        let viewController = GenderViewController()
        let router = GenderRouter(viewController)
        let presenter = GenderPresenter(view: viewController,
                                        router: router,
                                        isOnboarding: isOnboarding,
                                        delegate: delegate)
        viewController.dataSource = presenter
        viewController.input = presenter
        
        return viewController
    }
}
