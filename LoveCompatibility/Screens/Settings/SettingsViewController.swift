//
//  SettingsViewController.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import PinLayout

protocol SettingsDataSource: class {
    var cells: [SettingsPresenter.Cell] { get }
}

protocol SettingsInputDelegate: BaseViewDelegate {
    func removeAddCellDidTap()
    func rateUsCellDidTap()
    func dateCellDidTap()
    func genderCellDidTap()
    func shareDidTap()
    func updateData()
}

final class SettingsViewController: BaseViewController {
    // MARK: Properties
    var dataSource: SettingsDataSource?
    var input: SettingsInputDelegate?
    
    // MARK: UI
    private let tableView: UITableView = {
        let view = UITableView(frame: .zero, style: .plain)
        view.separatorStyle = .none
        view.backgroundColor = .clear
        view.showsVerticalScrollIndicator = false
        view.register(SettingsCell.self, forCellReuseIdentifier: SettingsCell.identifier)
        view.register(SpaceCell.self, forCellReuseIdentifier: SpaceCell.identifier)
        view.register(TitleCell.self, forCellReuseIdentifier: TitleCell.identifier)
        view.register(SwitchSettingsCell.self, forCellReuseIdentifier: SwitchSettingsCell.identifier)
        return view
    }()
    
    // MARK: Setup
    override func addSubviews() {
        view.addSubview(tableView)
    }
    
    override func setupLayout() {
        tableView.pin
            .vertically()
            .horizontally(30)
    }
    
    override func setupAppearance() {
        view.backgroundColor = R.color.background()
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
    }
}

// MARK: - SettingsView
extension SettingsViewController: SettingsView {
    func setDarkModeEnabled(_ isOn: Bool) {
        view.window?.overrideUserInterfaceStyle = isOn ? .dark : .light
    }
}

// MARK: - UITableViewDataSource
extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.cells.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = dataSource?.cells[indexPath.row] else { return UITableViewCell() }
        
        switch cell {
        case let .title(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: TitleCell.identifier) as! TitleCell
            cell.configure(with: model)
            return cell
        case let .settings(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsCell.identifier) as! SettingsCell
            cell.configure(with: model)
            return cell
        case let .space(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: SpaceCell.identifier) as! SpaceCell
            cell.configure(with: model)
            return cell
        case let .switchSettings(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: SwitchSettingsCell.identifier) as! SwitchSettingsCell
            cell.configure(with: model)
            return cell
        }
    }
}

// MARK: - UITableViewDelegate
extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = dataSource?.cells[indexPath.row] else { return }
        
        if case .settings(let model) = cell {
            switch model.type {
            case .upgradeToPremium:
                input?.removeAddCellDidTap()
            case .rateUs:
                input?.rateUsCellDidTap()
            case .share:
                input?.shareDidTap()
            case .gender:
                input?.genderCellDidTap()
            case .date:
                input?.dateCellDidTap()
            }
        }
    }
}

// MARK: - SettingsDelegate
extension SettingsViewController: SettingsDelegate {
    func update() {
        input?.updateData()
        tableView.reloadData()
        
        if let navCont = tabBarController?.viewControllers?[1] as? UINavigationController,
           let controller = navCont.viewControllers.first as? CompatibilityViewController {
            controller.update()
        }
    }
}
