//
//  SettingsCell.swift
//  LoveCompatibility
//
//  Created by Kirill on 14.02.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import PinLayout

final class SettingsCell: BaseTableViewCell {
    // MARK: UI
    private let containerView = UIView()
    private let titleLabel = UILabel()
    private let resultLabel = UILabel()
    private let emojiLabel = UILabel()
    private let dividerView = UIView()
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: 45)
    }
    
    // MARK: Setup
    override func addSubviews() {
        containerView.addSubview(titleLabel)
        containerView.addSubview(resultLabel)
        containerView.addSubview(emojiLabel)
        containerView.addSubview(dividerView)
        contentView.addSubview(containerView)
    }
    
    override func setupAppearance() {
        super.setupAppearance()
        
        titleLabel.font = R.font.sfProDisplayMedium(size: 20)
        titleLabel.textColor = R.color.main_text()
        
        resultLabel.font = R.font.sfProDisplayLight(size: 17)
        resultLabel.textColor = R.color.sub_text()

        emojiLabel.font = R.font.sfProDisplayMedium(size: 25)
        emojiLabel.textAlignment = .center
        
        dividerView.backgroundColor = R.color.cell_separator()
        
        containerView.backgroundColor = R.color.cell_background()
        containerView.layer.cornerRadius = 15
    }
    
    override func setupLayout() {
        containerView.pin
            .all()
        
        emojiLabel.pin
            .vertically()
            .left(2)
            .aspectRatio(1/1)
        
        resultLabel.pin
            .right(15)
            .vertically()
            .sizeToFit(.height)
        
        titleLabel.pin
            .right(of: emojiLabel)
            .horizontallyBetween(emojiLabel, and: resultLabel)
            .vertically()
        
        dividerView.pin
            .bottom()
            .height(1)
            .horizontally(30)
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
        titleLabel.text = model.title
        emojiLabel.text = model.emoji
        resultLabel.text = model.result
        setupCorners(position: model.position)
    }
    
    struct Model {
        let title: String
        let emoji: String?
        let result: String?
        let position: PositionInSection
        let type: SettingsType
        
        enum SettingsType {
            case upgradeToPremium
            case rateUs
            case share
            case gender
            case date
        }
        
        enum PositionInSection {
            case first
            case middle
            case last
            case single
        }
    }
}

// MARK: Private functions
private extension SettingsCell {
    private func setupCorners(position: Model.PositionInSection) {
        switch position {
        case .first:
            containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        case .middle:
            containerView.layer.cornerRadius = 0
        case .last:
            dividerView.isHidden = true
            containerView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        case .single:
            dividerView.isHidden = true
        }
    }
}
