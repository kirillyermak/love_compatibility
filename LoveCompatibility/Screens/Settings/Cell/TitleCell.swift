//
//  TitleCell.swift
//  LoveCompatibility
//
//  Created by Kirill on 14.02.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import PinLayout

final class TitleCell: BaseTableViewCell {
    // MARK: UI
    private let titleLabel = UILabel()
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: 45)
    }
    
    // MARK: Setup
    override func addSubviews() {
        contentView.addSubview(titleLabel)
    }
    
    override func setupAppearance() {
        super.setupAppearance()
        
        titleLabel.textColor = R.color.main_text()
        titleLabel.font = R.font.sfProDisplayBold(size: 40)
        titleLabel.textAlignment = .center
    }
    
    override func setupLayout() {
        titleLabel.pin
            .all()
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
        titleLabel.text = model.title
    }
    
    struct Model {
        let title: String
    }
}
