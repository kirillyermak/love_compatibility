//
//  SwitchSettingsCell.swift
//  LoveCompatibility
//
//  Created by Kirill on 18.02.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import PinLayout

protocol SwitchSettingsCellDelegate: class {
    func switchValueChanged(type: SwitchSettingsCell.Model.SettingsType, isOn: Bool)
}

final class SwitchSettingsCell: BaseTableViewCell {
    // MARK: Properties
    private var type: Model.SettingsType!
    private weak var delegate: SwitchSettingsCellDelegate?
    
    // MARK: UI
    private let containerView = UIView()
    private let titleLabel = UILabel()
    private let switchView = UISwitch()
    private let emojiLabel = UILabel()
    private let dividerView = UIView()
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: 45)
    }
    
    // MARK: Setup
    override func addSubviews() {
        containerView.addSubview(titleLabel)
        containerView.addSubview(switchView)
        containerView.addSubview(emojiLabel)
        containerView.addSubview(dividerView)
        contentView.addSubview(containerView)
    }
    
    override func setupAppearance() {
        super.setupAppearance()
        
        titleLabel.font = R.font.sfProDisplayMedium(size: 19)
        titleLabel.textColor = R.color.main_text()

        emojiLabel.font = R.font.sfProDisplayMedium(size: 25)
        emojiLabel.textAlignment = .center
        
        dividerView.backgroundColor = R.color.cell_separator()
        
        containerView.backgroundColor = R.color.cell_background()
        containerView.layer.cornerRadius = 15
    }
    
    override func setupLayout() {
        containerView.pin
            .all()
        
        emojiLabel.pin
            .vertically()
            .left(2)
            .aspectRatio(1/1)
        
        switchView.pin
            .right(15)
            .vCenter()
        
        titleLabel.pin
            .right(of: emojiLabel)
            .horizontallyBetween(emojiLabel, and: switchView)
            .vertically()
        
        dividerView.pin
            .bottom()
            .height(1)
            .horizontally(30)
    }
    
    override func setupActions() {
        switchView.addTarget(self, action: #selector(switchViewValueChanged), for: .valueChanged)
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
        titleLabel.text = model.title
        emojiLabel.text = model.emoji
        switchView.isOn = model.isOn
        type = model.type
        delegate = model.delegate
        setupCorners(position: model.position)
    }
    
    struct Model {
        let title: String
        let emoji: String?
        let result: String?
        let position: PositionInSection
        let isOn: Bool
        let type: SettingsType
        let delegate: SwitchSettingsCellDelegate?
        
        enum SettingsType {
            case notifications
            case darkMode
        }
        
        enum PositionInSection {
            case first
            case middle
            case last
            case single
        }
    }
}

// MARK: Private functions
private extension SwitchSettingsCell {
    private func setupCorners(position: Model.PositionInSection) {
        switch position {
        case .first:
            containerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        case .middle:
            containerView.layer.cornerRadius = 0
        case .last:
            dividerView.isHidden = true
            containerView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        case .single:
            dividerView.isHidden = true
        }
    }
    
    @objc private func switchViewValueChanged() {
        delegate?.switchValueChanged(type: type, isOn: switchView.isOn)
    }
}
