//
//  SettingsConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class SettingsConfigurator {
    static func configure() -> UIViewController {
        let viewController = SettingsViewController()
        let router = SettingsRouter(viewController)
        let presenter = SettingsPresenter(view: viewController,
                                          router: router)
        viewController.dataSource = presenter
        viewController.input = presenter
        
        return viewController
    }
}
