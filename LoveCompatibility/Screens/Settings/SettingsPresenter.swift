//
//  SettingsPresenter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation
import Haptico

protocol SettingsDelegate: class {
    func update()
}

protocol SettingsView: SettingsDelegate {
    func setDarkModeEnabled(_ isOn: Bool)
    func update()
}

final class SettingsPresenter {
    private unowned let view: SettingsView
    private let router: SettingsRouter
    private var fetchedCells: [Cell] = []
    
    init(view: SettingsView, router: SettingsRouter) {
        self.view = view
        self.router = router
        fetchCells()
    }
}

// MARK: SettingsDataSource
extension SettingsPresenter: SettingsDataSource {
    var cells: [Cell] {
        return fetchedCells
    }
}

// MARK: SettingsInputDelegate
extension SettingsPresenter: SettingsInputDelegate {
    func removeAddCellDidTap() {
        router.toSellingScreen()
    }
    
    func rateUsCellDidTap() {
        router.toRateScreen()
    }
    
    func dateCellDidTap() {
        router.toAgeScreen(delegate: view)
    }
    
    func genderCellDidTap() {
        router.toGenderScreen(delegate: view)
    }
    
    func shareDidTap() {
        router.toShareScreen()
    }
    
    func updateData() {
        fetchCells()
    }
}

// MARK: BaseViewDelegate
extension SettingsPresenter: BaseViewDelegate {
    func viewWillAppear() {
        fetchCells()
    }
}

// MARK: Enumerations
extension SettingsPresenter {
    enum Cell {
        case title(TitleCell.Model)
        case settings(SettingsCell.Model)
        case space(SpaceCell.Model)
        case switchSettings(SwitchSettingsCell.Model)
    }
}

// MARK: Private functions
private extension SettingsPresenter {
    private func fetchCells() {
        var cells: [Cell] = []
        
        cells.append(.space(.init(height: 10)))
        cells.append(.title(.init(title: R.string.localizable.settingsTitle())))
        cells.append(.space(.init(height: 10)))
        let currentSign = AppPreferences.shared.zodiacSign?.emoji
        cells.append(.settings(.init(title: R.string.localizable.settingsSign(),
                                     emoji: currentSign,
                                     result: AppPreferences.shared.zodiacSign?.title,
                                     position: .first,
                                     type: .date)))
        let currentGender = AppPreferences.shared.gender
        cells.append(.settings(.init(title: R.string.localizable.settingsGender(),
                                     emoji: currentGender == .female ? "🙋‍♀️" : "🙋‍♂️",
                                     result: nil,
                                     position: .last,
                                     type: .gender)))
        cells.append(.space(.init(height: 20)))
        if PurchaseManager.shared.isPremium == false {
            cells.append(.settings(.init(title: R.string.localizable.settingsUpgradePremium(),
                                         emoji: currentGender == .female ? "👸" : "🤴",
                                         result: nil,
                                         position: .single,
                                         type: .upgradeToPremium)))
            cells.append(.space(.init(height: 20)))
        }
        cells.append(.switchSettings(.init(title: R.string.localizable.settingsNotifications(),
                                     emoji: "✉️",
                                     result: nil,
                                     position: .single,
                                     isOn: AppPreferences.shared.isNotificationsEnabled,
                                     type: .notifications,
                                     delegate: self)))
        cells.append(.space(.init(height: 20)))
        cells.append(.switchSettings(.init(title: R.string.localizable.settingsDarkMode(),
                                     emoji: "🌙",
                                     result: nil,
                                     position: .single,
                                     isOn: AppPreferences.shared.isDarkModeEnabled,
                                     type: .darkMode,
                                     delegate: self)))
        cells.append(.space(.init(height: 20)))
        cells.append(.settings(.init(title: R.string.localizable.settingsShareWithFriends(),
                                     emoji: "✌️",
                                     result: nil,
                                     position: .first,
                                     type: .share)))
        cells.append(.settings(.init(title: R.string.localizable.settingsRate(),
                                     emoji: "✨",
                                     result: nil,
                                     position: .last,
                                     type: .rateUs)))
        cells.append(.space(.init(height: 20)))
        
        fetchedCells = cells
    }
}

// MARK: SwitchSettingsCellDelegate
extension SettingsPresenter: SwitchSettingsCellDelegate {
    func switchValueChanged(type: SwitchSettingsCell.Model.SettingsType, isOn: Bool) {
        switch type {
        case .darkMode:
            if PurchaseManager.shared.isPremium {
                AppPreferences.shared.isDarkModeEnabled = isOn
                view.setDarkModeEnabled(isOn)
            } else {
                Haptico.shared().generate(.error)
                router.toSellingScreen()
                view.update()
            }
        case .notifications:
            PushNotificationsManager.shared.requestAuthorization { [weak self] (isEnabled) in
                if isEnabled {
                    PushNotificationsManager.shared.scheduledMorningNotifications()
                } else {
                    Haptico.shared().generate(.error)
                    
                    DispatchQueue.main.async {
                        self?.router.toNotificationsAlert()
                        self?.view.update()
                    }
                }
            }
        }
    }
}
