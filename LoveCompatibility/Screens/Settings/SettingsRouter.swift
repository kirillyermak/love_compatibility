//
//  SettingsRouter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import StoreKit

final class SettingsRouter: Router<SettingsViewController> {
    func toRateScreen() {
        SKStoreReviewController.requestReview()
    }
    
    func toSellingScreen() {
        let controller = SellingConfigurator.configure()
        viewController?.present(controller, animated: true, completion: nil)
    }
    
    func toAgeScreen(delegate: SettingsDelegate?) {
        let controller = AgeConfigurator.configure(isOnboarding: false, delegate: delegate)
        viewController?.present(controller, animated: true, completion: nil)
    }
    
    func toGenderScreen(delegate: SettingsDelegate?) {
        let controller = GenderConfigurator.configure(isOnboarding: false, delegate: delegate)
        viewController?.present(controller, animated: true, completion: nil)
    }
    
    func toShareScreen() {
        let controller = ShareManager.shared.share()
        viewController?.present(controller, animated: true, completion: nil)
    }
    
    func toNotificationsAlert() {
        let alert = UIAlertController(title: "Уведомления отключены", message: "Надо включить их в настройках", preferredStyle: .alert)
        alert.addAction(.init(title: R.string.localizable.commonYes(), style: .default, handler: { (_) in
            guard
                let bundleIdentifier = Bundle.main.bundleIdentifier,
                let appSettings = URL(string: UIApplication.openSettingsURLString + bundleIdentifier),
                UIApplication.shared.canOpenURL(appSettings)
            else { return }
            
            UIApplication.shared.open(appSettings)
        }))
        alert.addAction(.init(title: R.string.localizable.commonNo(), style: .cancel, handler: nil))
        viewController?.present(alert, animated: true, completion: nil)
    }
}
