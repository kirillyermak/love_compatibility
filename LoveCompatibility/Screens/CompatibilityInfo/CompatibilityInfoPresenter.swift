//
//  CompatibilityInfoPresenter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol CompatibilityInfoView: class {}

final class CompatibilityInfoPresenter {
    private unowned let view: CompatibilityInfoView
    private let router: CompatibilityInfoRouter
    private var compatibilityInfo: Compatibility?
    private var fetchedCells: [Cell] = []
    private let pair: ZodiacPair
    
    init(view: CompatibilityInfoView, router: CompatibilityInfoRouter, pair: ZodiacPair) {
        self.view = view
        self.router = router
        self.pair = pair
        
        if AppPreferences.shared.gender == .female {
            compatibilityInfo = CompatibilitiesManager.shared.fetchFor(female: pair.user, male: pair.partner)
        } else {
            compatibilityInfo = CompatibilitiesManager.shared.fetchFor(female: pair.partner, male: pair.user)
        }
        
        fetchCells()
    }
}

// MARK: CompatibilityInfoDataSource
extension CompatibilityInfoPresenter: CompatibilityInfoDataSource {
    var cells: [Cell] {
        return fetchedCells
    }
}

// MARK: CompatibilityInfoInputDelegate
extension CompatibilityInfoPresenter: CompatibilityInfoInputDelegate {}

// MARK: BaseViewDelegate
extension CompatibilityInfoPresenter: BaseViewDelegate {}

// MARK: Private functions
private extension CompatibilityInfoPresenter {
    private func fetchCells() {
        guard let info = compatibilityInfo else { return }
        
        var cells: [Cell] = []
        let isLocked = PurchaseManager.shared.isPremium == false
        
        cells.append(.close(.init(delegate: self)))
        cells.append(.space(.init(height: 20)))
        cells.append(.percent(.init(percent: info.compatibility)))
        cells.append(.space(.init(height: 20)))
        cells.append(.description(.init(description: info.description)))
        cells.append(.space(.init(height: 20)))
        cells.append(.categorizedInfo(.init(emoji: "🤝", title: R.string.localizable.compatibilityInfoFriendship(), info: info.friendship, isLocked: false)))
        cells.append(.space(.init(height: 15)))
        cells.append(.categorizedInfo(.init(emoji: "💼", title: R.string.localizable.compatibilityInfoWork(), info: info.work, isLocked: false)))
        cells.append(.space(.init(height: 15)))
        cells.append(.categorizedInfo(.init(emoji: "❤️", title: R.string.localizable.compatibilityInfoLove(), info: info.love, isLocked: isLocked)))
        cells.append(.space(.init(height: 15)))
        cells.append(.categorizedInfo(.init(emoji: "👨‍👩‍👧‍👦", title: R.string.localizable.compatibilityInfoFamily(), info: info.family, isLocked: isLocked)))
        cells.append(.space(.init(height: 15)))
        cells.append(.categorizedInfo(.init(emoji: "🤫", title: R.string.localizable.compatibilityInfoSex(), info: info.sex, isLocked: isLocked)))
        cells.append(.space(.init(height: 35)))
        if isLocked {
            cells.append(.unlockAll(.init(delegate: self)))
            cells.append(.space(.init(height: 35)))
        }
        cells.append(.share(.init(delegate: self, compatibilityPercent: info.compatibility)))
        cells.append(.space(.init(height: 35)))
        
        fetchedCells = cells
    }
}

// MARK: - Enumetarions
extension CompatibilityInfoPresenter {
    enum Cell {
        case close(CloseCell.Model)
        case share(ShareCell.Model)
        case description(CompatibilityDescriptionCell.Model)
        case percent(CompabilityPercentCell.Model)
        case categorizedInfo(CategorizedInfoCell.Model)
        case space(SpaceCell.Model)
        case unlockAll(UnlockAllCell.Model)
    }
}

// MARK: - CloseCellDelegate
extension CompatibilityInfoPresenter: CloseCellDelegate {
    func closeButtonDidTap() {
        router.toBack()
    }
}

// MARK: - ShareCellDelegate
extension CompatibilityInfoPresenter: ShareCellDelegate {
    func shareButtonDidTap() {
        guard let info = compatibilityInfo else { return }
        router.toShare(pair: pair, percent: info.compatibility)
    }
}

// MARK: - CategorizedInfoCellDelegate
extension CompatibilityInfoPresenter: UnlockAllCellDelegate {
    func unlockAllButtonTap() {
        router.toSellingScreen()
    }
}
