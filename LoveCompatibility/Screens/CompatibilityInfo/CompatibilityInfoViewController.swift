//
//  CompatibilityInfoViewController.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import PinLayout

protocol CompatibilityInfoDataSource: class {
    var cells: [CompatibilityInfoPresenter.Cell] { get }
}

protocol CompatibilityInfoInputDelegate: BaseViewDelegate {}

final class CompatibilityInfoViewController: BaseViewController {
    // MARK: Properties
    var dataSource: CompatibilityInfoDataSource?
    var input: CompatibilityInfoInputDelegate?
    
    // MARK: UI
    private let tableView: UITableView = {
        let view = UITableView(frame: .zero, style: .grouped)
        view.separatorStyle = .none
        view.estimatedRowHeight = 100
        view.rowHeight = UITableView.automaticDimension
        view.backgroundColor = .clear
        view.showsVerticalScrollIndicator = false
        view.clipsToBounds = false
        view.register(CompabilityPercentCell.self, forCellReuseIdentifier: CompabilityPercentCell.identifier)
        view.register(CategorizedInfoCell.self, forCellReuseIdentifier: CategorizedInfoCell.identifier)
        view.register(SpaceCell.self, forCellReuseIdentifier: SpaceCell.identifier)
        view.register(CompatibilityDescriptionCell.self, forCellReuseIdentifier: CompatibilityDescriptionCell.identifier)
        view.register(ShareCell.self, forCellReuseIdentifier: ShareCell.identifier)
        view.register(CloseCell.self, forCellReuseIdentifier: CloseCell.identifier)
        view.register(UnlockAllCell.self, forCellReuseIdentifier: UnlockAllCell.identifier)
        return view
    }()
    
    // MARK: Setup
    override func addSubviews() {
        view.addSubview(tableView)
    }
    
    override func setupLayout() {
        tableView.pin
            .vertically()
            .horizontally(30)
    }
    
    override func setupAppearance() {
        view.backgroundColor = R.color.background()
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
    }
}

// MARK: - CompatibilityInfoView
extension CompatibilityInfoViewController: CompatibilityInfoView {}

// MARK: - UITableViewDataSource
extension CompatibilityInfoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.cells.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = dataSource?.cells[indexPath.row] else { return UITableViewCell() }
        
        switch cell {
        case let .percent(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: CompabilityPercentCell.identifier) as! CompabilityPercentCell
            cell.configure(with: model)
            return cell
        case let .categorizedInfo(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: CategorizedInfoCell.identifier) as! CategorizedInfoCell
            cell.configure(with: model)
            return cell
        case let .space(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: SpaceCell.identifier) as! SpaceCell
            cell.configure(with: model)
            return cell
        case let .share(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: ShareCell.identifier) as! ShareCell
            cell.configure(with: model)
            return cell
        case let .description(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: CompatibilityDescriptionCell.identifier) as! CompatibilityDescriptionCell
            cell.configure(with: model)
            return cell
        case let .close(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: CloseCell.identifier) as! CloseCell
            cell.configure(with: model)
            return cell
        case let .unlockAll(model):
            let cell = tableView.dequeueReusableCell(withIdentifier: UnlockAllCell.identifier) as! UnlockAllCell
            cell.configure(with: model)
            return cell
        }
    }
}
