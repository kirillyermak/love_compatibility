//
//  CompabilityPercentCell.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import UIKit
import PinLayout

final class CompabilityPercentCell: BaseTableViewCell {
    // MARK: UI
    private let percentCountLabel = UILabel()
    private let percentLabel = UILabel()
    
    // MARK: Properties
    private let percentCountFont = R.font.sfProDisplayLight(size: 34)!
    private let percentFont = R.font.sfProDisplayRegular(size: 14)!
    
    // MARK: Setup
    override func addSubviews() {
        contentView.addSubview(percentCountLabel)
        contentView.addSubview(percentLabel)
    }
    
    override func setupLayout() {
        percentCountLabel.pin
            .vertically()
            .left()
            .sizeToFit(.height)
        
        percentLabel.pin
            .right(of: percentCountLabel)
            .bottom()
            .right()
            .sizeToFit(.width)
    }
    
    override func setupAppearance() {
        super.setupAppearance()
        
        let color = R.color.main_text()
        
        percentCountLabel.textColor = color
        percentCountLabel.font = percentCountFont
        
        percentLabel.textColor = color
        percentLabel.font = percentFont
    }
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let percentCountHeight = percentCountLabel.text?.height(withConstrainedWidth: size.width, font: percentCountFont) ?? 0
        return CGSize(width: size.width, height: percentCountHeight)
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
            
        percentCountLabel.text = "Совместимы на " + String(model.percent)
        percentLabel.text = "%"
    }
    
    struct Model {
        let percent: Int
    }
}
