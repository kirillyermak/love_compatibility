//
//  UnlockAllCell.swift
//  LoveCompatibility
//
//  Created by Kirill on 02.03.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import UIKit
import PinLayout
import Lottie

protocol UnlockAllCellDelegate: class {
    func unlockAllButtonTap()
}

final class UnlockAllCell: BaseTableViewCell {
    // MARK: UI
    private let unlockButton = UIButton()
    private let animationView = AnimationView()
    
    // MARK: Properties
    private weak var delegate: UnlockAllCellDelegate?
    
    // MARK: Setup
    override func addSubviews() {
        contentView.addSubview(animationView)
        contentView.addSubview(unlockButton)
    }
    
    override func setupLayout() {
        unlockButton.pin
            .all()
        
        animationView.pin
            .width(unlockButton.frame.width * 2)
            .height(unlockButton.frame.height * 3)
            .center(to: unlockButton.anchor.center)
    }
    
    override func setupAppearance() {
        super.setupAppearance()
        
        unlockButton.titleLabel?.font = R.font.sfProDisplayBold(size: 20)
        let gender = AppPreferences.shared.gender
        let title = "\(gender == .female ? " 👸🏽" : " 🤴🏽") \(R.string.localizable.compatibilityInfoUnlockAll().uppercased())"
        unlockButton.setTitle(title, for: .normal)
        unlockButton.setTitleColor(R.color.dark_main_text(), for: .normal)
        
        let animation = Animation.named("big_btn_green")
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.play()
    }
    
    override func setupActions() {
        unlockButton.addTarget(self, action: #selector(unlockButtonAction), for: .touchUpInside)
    }
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: 45)
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
        delegate = model.delegate
    }
    
    struct Model {
        let delegate: UnlockAllCellDelegate?
    }
}

private extension UnlockAllCell {
    @objc private func unlockButtonAction() {
        delegate?.unlockAllButtonTap()
    }
}
