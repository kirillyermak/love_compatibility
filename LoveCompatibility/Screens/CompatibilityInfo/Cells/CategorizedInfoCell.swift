//
//  CategorizedInfoCell.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import UIKit
import PinLayout
import Lottie

protocol CategorizedInfoCellDelegate: class {
    func unlockAllButtonTap()
}

final class CategorizedInfoCell: BaseTableViewCell {
    // MARK: UI
    private let titleLabel = UILabel()
    private let separatorView = UIView()
    private let infoLabel = UILabel()
    private let unlockButton = UIButton()
    
    // MARK: Properties
    private let titleFont = R.font.sfProDisplayBold(size: 19)!
    private let infoFont = R.font.sfProDisplayRegular(size: 16)!
    private let padding: CGFloat = 20
    private let separatorHeight: CGFloat = 4
    
    // MARK: Setup
    override func addSubviews() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(separatorView)
        contentView.addSubview(infoLabel)
    }
    
    override func setupLayout() {
        titleLabel.pin
            .top()
            .horizontally()
            .sizeToFit(.width)
        
        separatorView.pin
            .below(of: titleLabel)
            .height(separatorHeight)
            .left()
            .width(75%)
            .marginTop(5)
        
        infoLabel.pin
            .below(of: separatorView)
            .horizontally()
            .marginTop(padding)
            .bottom()
    }
    
    override func setupAppearance() {
        super.setupAppearance()
        
        let color = R.color.blue_text()
        
        titleLabel.font = titleFont
        titleLabel.textColor = color
        
        separatorView.backgroundColor = color
        
        infoLabel.numberOfLines = 0
        infoLabel.font = infoFont
        infoLabel.textColor = R.color.main_text()
    }
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let titleHeight = titleLabel.text?.height(withConstrainedWidth: size.width, font: titleFont) ?? 0
        let infoHeight = infoLabel.text?.height(withConstrainedWidth: size.width, font: infoFont) ?? 0
        
        let totalHeight = titleHeight + 5 + separatorHeight + padding + infoHeight
        
        return CGSize(width: size.width, height: totalHeight)
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
        
        if model.isLocked {
            let gender = AppPreferences.shared.gender
            titleLabel.text = model.title.uppercased() + (gender == .female ? " 👸" : " 🤴")
        } else {
            titleLabel.text = model.emoji + " " + model.title.uppercased()
        }
        
        if model.isLocked {
            infoLabel.alpha = 0.2
            infoLabel.text = String(model.info.prefix(70)) + "..."
        } else {
            infoLabel.alpha = 1
            infoLabel.text = model.info
        }
    }
    
    struct Model {
        let emoji: String
        let title: String
        let info: String
        let isLocked: Bool
    }
}
