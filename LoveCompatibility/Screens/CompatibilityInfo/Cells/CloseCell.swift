//
//  CloseCell.swift
//  LoveCompatibility
//
//  Created by Kirill on 01.03.2021.
//  Copyright © 2021 kyrylyermak. All rights reserved.
//

import UIKit
import PinLayout

protocol CloseCellDelegate: class {
    func closeButtonDidTap()
}

final class CloseCell: BaseTableViewCell {
    // MARK: UI
    private let closeButton = UIButton()
    
    // MARK: Properties
    private weak var delegate: CloseCellDelegate?
    
    // MARK: Setup
    override func addSubviews() {
        contentView.addSubview(closeButton)
    }
    
    override func setupLayout() {
        closeButton.pin
            .right()
            .height(120%)
            .aspectRatio(1/1)
        
        closeButton.layer.cornerRadius = closeButton.frame.height / 2
    }
    
    override func setupAppearance() {
        super.setupAppearance()
        
        closeButton.backgroundColor = R.color.secondary_background_light()
        closeButton.setImage(R.image.x()?.withRenderingMode(.alwaysOriginal), for: .normal)
    }
    
    override func setupActions() {
        closeButton.addTarget(self, action: #selector(closeButtonAction), for: .touchUpInside)
    }
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: 24)
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
        delegate = model.delegate
    }
    
    struct Model {
        let delegate: CloseCellDelegate
    }
}

private extension CloseCell {
    @objc private func closeButtonAction() {
        delegate?.closeButtonDidTap()
    }
}
