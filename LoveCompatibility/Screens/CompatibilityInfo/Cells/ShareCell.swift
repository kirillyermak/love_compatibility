//
//  ShareCell.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 27.01.2021.
//  Copyright © 2021 Test. All rights reserved.
//

import UIKit
import PinLayout

protocol ShareCellDelegate: class {
    func shareButtonDidTap()
}

final class ShareCell: BaseTableViewCell {
    // MARK: UI
    private let containerView = UIView()
    private let shareLabel = UILabel()
    private let shareButton = UIButton()
    
    // MARK: Properties
    private weak var delegate: ShareCellDelegate?
    
    // MARK: Setup
    override func addSubviews() {
        containerView.addSubview(shareButton)
        containerView.addSubview(shareLabel)
        contentView.addSubview(containerView)
    }
    
    override func setupLayout() {
        containerView.pin
            .all()
        
        shareButton.pin
            .bottom(20)
            .height(34)
            .horizontally(30)
        
        shareLabel.pin
            .above(of: shareButton)
            .marginBottom(15)
            .top(15)
            .horizontally(30)
    }
    
    override func setupAppearance() {
        super.setupAppearance()
        
        containerView.layer.cornerRadius = 13
        containerView.backgroundColor = R.color.secondary_background()
        
        shareLabel.font = R.font.sfProDisplayRegular(size: 18)
        shareLabel.textColor = R.color.main_text()
        shareLabel.textAlignment = .center
        
        shareButton.setTitleColor(R.color.light_main_text(), for: .normal)
        shareButton.titleLabel?.font = R.font.sfProDisplayBold(size: 18)
        shareButton.setTitle(R.string.localizable.compatibilityInfoShare(), for: .normal)
        shareButton.backgroundColor = R.color.button_purple()
        shareButton.layer.cornerRadius = 13
    }
    
    override func setupActions() {
        shareButton.addTarget(self, action: #selector(shareButtonAction), for: .touchUpInside)
    }
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: 110)
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
        delegate = model.delegate
        
        var emoji = ""
        
        switch model.compatibilityPercent {
        case 0...70:
            emoji = "🤝"
        case 71...80:
            emoji = "👍"
        case 81...90:
            emoji = "🤗"
        default:
            emoji = "🥰"
        }
        
        shareLabel.text = "\(R.string.localizable.compatibilityInfoCompatibleFor()) \(model.compatibilityPercent)%! \(emoji)"
    }
    
    struct Model {
        let delegate: ShareCellDelegate
        let compatibilityPercent: Int
    }
}

private extension ShareCell {
    @objc private func shareButtonAction() {
        delegate?.shareButtonDidTap()
    }
}
