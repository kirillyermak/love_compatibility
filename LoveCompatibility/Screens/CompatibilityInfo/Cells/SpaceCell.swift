//
//  SpaceCell.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import UIKit
import PinLayout

final class SpaceCell: BaseTableViewCell {
    // MARK: Properties
    private var height: CGFloat = 0
    
    // MARK: Setup
    override func setupAppearance() {
        super.setupAppearance()
    }
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: height)
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
        height = model.height
    }
    
    struct Model {
        let height: CGFloat
    }
}
