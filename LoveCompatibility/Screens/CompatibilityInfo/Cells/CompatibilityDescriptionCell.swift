//
//  CompatibilityDescriptionCell.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 27.01.2021.
//  Copyright © 2021 Test. All rights reserved.
//

import UIKit
import PinLayout

final class CompatibilityDescriptionCell: BaseTableViewCell {
    // MARK: UI
    private let descriptionLabel = UILabel()
    
    // MARK: Properties
    private let descriptionFont = R.font.sfProDisplayRegular(size: 16)!
    
    // MARK: Setup
    override func addSubviews() {
        contentView.addSubview(descriptionLabel)
    }
    
    override func setupLayout() {
        descriptionLabel.pin
            .all()
    }
    
    override func setupAppearance() {
        super.setupAppearance()
        
        descriptionLabel.font = descriptionFont
        descriptionLabel.textColor = R.color.main_text()
        descriptionLabel.numberOfLines = 0
    }
    
    // MARK: Lifecycle
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let labelHeight = descriptionLabel.text?.height(withConstrainedWidth: size.width, font: descriptionFont) ?? 0
        return CGSize(width: size.width, height: labelHeight)
    }
    
    // MARK: Configure
    func configure(with model: Model) {
        super.configure()
        descriptionLabel.text = model.description
    }
    
    struct Model {
        let description: String
    }
}
