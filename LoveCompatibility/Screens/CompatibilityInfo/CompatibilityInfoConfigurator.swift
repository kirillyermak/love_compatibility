//
//  CompatibilityInfoConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class CompatibilityInfoConfigurator {
    static func configure(pair: ZodiacPair) -> UIViewController {
        let viewController = CompatibilityInfoViewController()
        let router = CompatibilityInfoRouter(viewController)
        let presenter = CompatibilityInfoPresenter(view: viewController,
                                                   router: router,
                                                   pair: pair)
        viewController.dataSource = presenter
        viewController.input = presenter
        
        return viewController
    }
}
