//
//  CompatibilityInfoRouter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class CompatibilityInfoRouter: Router<CompatibilityInfoViewController> {
    func toBack() {
        viewController?.dismiss(animated: true, completion: nil)
    }
    
    func toSellingScreen() {
        let controller = SellingConfigurator.configure()
        viewController?.present(controller, animated: true, completion: nil)
    }
    
    func toShare(pair: ZodiacPair, percent: Int) {
        let controller = ShareManager.shared.share(pair: pair, percent: percent)
        viewController?.present(controller, animated: true, completion: nil)
    }
}
