//
//  SellingConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 25.01.2021.
//  Copyright (c) 2021 Test. All rights reserved.
//

import UIKit

final class SellingConfigurator {
    static func configure() -> UIViewController {
        let viewController = SellingViewController()
        let router = SellingRouter(viewController)
        let presenter = SellingPresenter(view: viewController,
                                         router: router)
        viewController.dataSource = presenter
        viewController.input = presenter
        
        return viewController
    }
}
