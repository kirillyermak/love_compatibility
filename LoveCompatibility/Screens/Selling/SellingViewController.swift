//
//  SellingViewController.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 25.01.2021.
//  Copyright (c) 2021 Test. All rights reserved.
//

import UIKit
import PinLayout
import Lottie

protocol SellingDataSource: class {}

protocol SellingInputDelegate: BaseViewDelegate {
    func buyButtonDidTap()
    func restoreButtonDidTap()
}

final class SellingViewController: BaseViewController {
    // MARK: Properties
    var dataSource: SellingDataSource?
    var input: SellingInputDelegate?
    
    private let advateges = [
        R.string.localizable.sellingAdvantage1(),
        R.string.localizable.sellingAdvantage2(),
        R.string.localizable.sellingAdvantage3(),
        R.string.localizable.sellingAdvantage4(),
        R.string.localizable.sellingAdvantage5()
    ]
    
    // MARK: UI
    private let titleImageView = UIImageView()
    private let titleLabel = UILabel()
    private let advantagesStackView = UIStackView()
    private let buyDescriptionLabel = UILabel()
    private let buyButton = UIButton()
    private let restoreButton = UIButton()
    private let animationView = AnimationView()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        input?.viewDidLoad()
    }
    
    // MARK: Setup
    override func addSubviews() {
        view.addSubview(animationView)
        view.addSubview(titleImageView)
        view.addSubview(titleLabel)
        fillAndAddToSubviewsAdvantagesStackView()
        view.addSubview(advantagesStackView)
        view.addSubview(buyDescriptionLabel)
        view.addSubview(animationView)
        view.addSubview(buyButton)
        view.addSubview(restoreButton)
    }
    
    override func setupLayout() {
        titleImageView.pin
            .top()
            .horizontally()
            .height(250)
        
        buyButton.pin
            .bottom(50 + view.pin.safeArea.bottom)
            .horizontally(30)
            .height(47)
        
        animationView.pin
            .center(to: buyButton.anchor.center)
            .height(buyButton.frame.height * 3)
            .width(buyButton.frame.width * 2)
        
        restoreButton.pin
            .below(of: buyButton)
            .width(of: buyButton)
            .marginTop(10)
            .hCenter()
            .height(25)
        
        titleLabel.pin
            .below(of: titleImageView)
            .horizontally()
            .sizeToFit(.width)
        
        advantagesStackView.pin
            .verticallyBetween(titleLabel, and: buyButton)
            .horizontally(30)
            .marginTop(15)
        
        buyDescriptionLabel.pin
            .above(of: buyButton)
            .marginBottom(10)
            .horizontally(30)
            .sizeToFit(.width)
    }
    
    override func setupAppearance() {
        view.backgroundColor = R.color.background()
        
        titleImageView.image = R.image.selling_image()
        titleImageView.contentMode = .scaleAspectFit
        
        let gender = AppPreferences.shared.gender == .female ? "👸" : "🤴"
        
        titleLabel.text = "\(R.string.localizable.sellingAdvantagesOf()) \(gender)"
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.font = R.font.sfProDisplayBold(size: 40)
        titleLabel.textColor = R.color.main_text()
        
        buyButton.titleLabel?.font = R.font.sfProDisplayBold(size: 20)
        buyButton.setTitle("\(R.string.localizable.sellingBecome().uppercased())  \(gender)", for: .normal)
        buyButton.setTitleColor(R.color.dark_main_text(), for: .normal)
        
        restoreButton.setTitle(R.string.localizable.sellingRestore(), for: .normal)
        restoreButton.setTitleColor(R.color.sub_text(), for: .normal)
        restoreButton.titleLabel?.font = R.font.sfProDisplayThinItalic(size: 14)
        
        advantagesStackView.axis = .vertical
        advantagesStackView.spacing = 10
        
        buyDescriptionLabel.numberOfLines = 0
        buyDescriptionLabel.textAlignment = .center
        buyDescriptionLabel.textColor = R.color.orange_text()?.withAlphaComponent(0.7)
        
        let animation = Animation.named("big_btn_coral")
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.play()
    }
    
    override func setupActions() {
        buyButton.addTarget(self, action: #selector(buyButtonAction), for: .touchUpInside)
        restoreButton.addTarget(self, action: #selector(restoreButtonAction), for: .touchUpInside)
    }
}

// MARK: Button Actions
private extension SellingViewController {
    @objc private func buyButtonAction() {
        input?.buyButtonDidTap()
    }
    
    @objc private func restoreButtonAction() {
        input?.restoreButtonDidTap()
    }
}

// MARK: SellingView
extension SellingViewController: SellingView {
    func updateDescriptionWith(text: String) {
        buyDescriptionLabel.text = text
    }
}

// MARK: Private functions
private extension SellingViewController {
    private func fillAndAddToSubviewsAdvantagesStackView() {
        advateges.forEach { (text) in
            let label = UILabel()
            label.text = "✅ " + text
            label.font = UIFont.italicSystemFont(ofSize: 19)
            label.textColor = R.color.main_text()
            advantagesStackView.addArrangedSubview(label)
        }
        
        advantagesStackView.addArrangedSubview(UIView())
    }
}
