//
//  SellingRouter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 25.01.2021.
//  Copyright (c) 2021 Test. All rights reserved.
//

import UIKit

final class SellingRouter: Router<SellingViewController> {}
