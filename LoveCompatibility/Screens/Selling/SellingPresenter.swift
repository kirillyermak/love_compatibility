//
//  SellingPresenter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 25.01.2021.
//  Copyright (c) 2021 Test. All rights reserved.
//

protocol SellingView: class {
    func updateDescriptionWith(text: String)
}

final class SellingPresenter {
    private unowned let view: SellingView
    private let router: SellingRouter
    private let purchaseId = RCManager.shared.string(forKey: .purchaseId)
    
    init(view: SellingView, router: SellingRouter) {
        self.view = view
        self.router = router
    }
}

// MARK: SellingDataSource
extension SellingPresenter: SellingDataSource {}

// MARK: SellingInputDelegate
extension SellingPresenter: SellingInputDelegate {
    func buyButtonDidTap() {
        guard let purchaseId = self.purchaseId else { return }
        PurchaseManager.shared.purchaseProduct(id: purchaseId) { (isPurchased) in
            print(isPurchased)
        }
    }
    
    func restoreButtonDidTap() {
        PurchaseManager.shared.restore { (isPurchased) in
            print(isPurchased)
        }
    }
}

// MARK: BaseViewDelegate
extension SellingPresenter: BaseViewDelegate {
    func viewDidLoad() {
        guard
            let purchaseId = self.purchaseId,
            let productInfo = PurchaseManager.shared.premiumProductsInfo?.first(where: { $0.productId == purchaseId })
        else {
            let text = R.string.localizable.sellingWeekDescription() + "0 $" + "\n" + R.string.localizable.sellingCancelAnytime()
            view.updateDescriptionWith(text: text)
            return
        }
        
        var sellingDesc = ""
        
        switch purchaseId {
        case PurchaseId.weekTrial:
            sellingDesc = R.string.localizable.sellingWeekDescription()
        case PurchaseId.monthlyTrial:
            sellingDesc = R.string.localizable.sellingMonthDescription()
        case PurchaseId.yearlyTrial:
            sellingDesc = R.string.localizable.sellingYearDescription()
        default:
            return
        }
        
        let text = sellingDesc + (productInfo.price ?? "0 $") + "\n" + R.string.localizable.sellingCancelAnytime()
        view.updateDescriptionWith(text: text)
    }
}

// MARK: Private functions
private extension SellingPresenter {}
