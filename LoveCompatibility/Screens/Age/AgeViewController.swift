//
//  AgeViewController.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import PinLayout
import Lottie

protocol AgeDataSource: class {}

protocol AgeInputDelegate: BaseViewDelegate {
    func datePickerDidChangeDate(_ date: Date)
    func nextButtonDidTap()
}

final class AgeViewController: BaseViewController {
    // MARK: Properties
    var dataSource: AgeDataSource?
    var input: AgeInputDelegate?
    
    // MARK: UI
    private let zodiacSignCircleImageView = UIImageView()
    private let shadowView = UIView()
    private let zodiacSignLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let datePicker = UIDatePicker()
    private let nextButton = UIButton()
    private let animationView = AnimationView()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        input?.viewDidLoad()
    }
    
    // MARK: Setup
    override func addSubviews() {
        view.addSubview(zodiacSignCircleImageView)
        view.addSubview(shadowView)
        view.addSubview(zodiacSignLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(datePicker)
        view.addSubview(animationView)
        view.addSubview(nextButton)
    }
    
    override func setupLayout() {
        zodiacSignLabel.pin
            .top(20 + view.pin.safeArea.top)
            .horizontally(30)
            .sizeToFit(.width)
        
        zodiacSignCircleImageView.pin
            .below(of: zodiacSignLabel)
            .width(150%)
            .hCenter()
            .aspectRatio(1/1)
        
        nextButton.pin
            .bottom(30 + view.pin.safeArea.bottom)
            .horizontally(30)
            .height(47)
        
        animationView.pin
            .center(to: nextButton.anchor.center)
            .height(nextButton.frame.height * 3)
            .width(nextButton.frame.width * 2)
        
        datePicker.pin
            .above(of: nextButton)
            .horizontally(30)
            .marginBottom(20)
            .height(40%)
        
        descriptionLabel.pin
            .above(of: datePicker)
            .marginTop(30)
            .horizontally(30)
            .sizeToFit(.width)
        
        shadowView.pin
            .top(to: descriptionLabel.edge.top)
            .marginTop(-10)
            .bottom()
            .horizontally()
    }
    
    override func setupAppearance() {
        view.backgroundColor = R.color.background()
        
        zodiacSignCircleImageView.image = R.image.zodiac_circle()
        
        shadowView.backgroundColor = R.color.background()
        
        zodiacSignLabel.textAlignment = .center
        zodiacSignLabel.font = R.font.sfProDisplayBold(size: 40)
        zodiacSignLabel.textColor = R.color.blue_text()
        
        descriptionLabel.text = R.string.localizable.ageYourBirthDate()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .center
        descriptionLabel.textColor = R.color.sub_text()
        
        datePicker.date = AppPreferences.shared.birthDate ?? Date()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.maximumDate = Date()
        datePicker.tintColor = R.color.main_text()
        input?.datePickerDidChangeDate(datePicker.date)
        
        nextButton.titleLabel?.font = R.font.sfProDisplayBold(size: 20)
        nextButton.setTitleColor(R.color.dark_main_text(), for: .normal)
        
        let animation = Animation.named("big_btn_green")
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.play()
    }
    
    override func setupActions() {
        datePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        nextButton.addTarget(self, action: #selector(nextButtonAction), for: .touchUpInside)
    }
}

// MARK: Button Actions
private extension AgeViewController {
    @objc private func nextButtonAction() {
        input?.nextButtonDidTap()
    }
}

// MARK: AgeView
extension AgeViewController: AgeView {
    func updateZodiacLabelWith(zodiacSign: ZodiacSign) {
        zodiacSignLabel.text = zodiacSign.title
        zodiacSignLabel.sizeToFit()
        
        UIView.animate(withDuration: 0.4) { [weak self] in
            guard let `self` = self else { return }
            self.zodiacSignCircleImageView.transform = .identity
            let transform = self.zodiacSignCircleImageView.transform.rotated(by: self.degreesToRadians(zodiacSign.bottomAngle))
            self.zodiacSignCircleImageView.transform = transform
        }
    }
    
    func updateNextButtonWith(title: String) {
        nextButton.setTitle(title, for: .normal)
    }
    
    private func degreesToRadians(_ deg: CGFloat) -> CGFloat {
        return deg * CGFloat.pi / 180
    }
}

// MARK: Private functions
private extension AgeViewController {
    @objc func handleDatePicker(_ datePicker: UIDatePicker) {
        input?.datePickerDidChangeDate(datePicker.date)
    }
}
