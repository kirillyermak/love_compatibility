//
//  AgeConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class AgeConfigurator {
    static func configure(isOnboarding: Bool = true, delegate: SettingsDelegate? = nil) -> UIViewController {
        let viewController = AgeViewController()
        let router = AgeRouter(viewController)
        let presenter = AgePresenter(view: viewController,
                                     router: router,
                                     isOnboarding: isOnboarding,
                                     delegate: delegate)
        viewController.dataSource = presenter
        viewController.input = presenter
        
        return viewController
    }
}
