//
//  AgePresenter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import SwiftDate

protocol AgeView: class {
    func updateZodiacLabelWith(zodiacSign: ZodiacSign)
    func updateNextButtonWith(title: String)
}

final class AgePresenter {
    private unowned let view: AgeView
    private let router: AgeRouter
    private let isOnboarding: Bool
    private var selectedZodiacSign: ZodiacSign?
    private var selectedDate: Date?
    private weak var delegate: SettingsDelegate?
    
    init(view: AgeView, router: AgeRouter, isOnboarding: Bool, delegate: SettingsDelegate?) {
        self.view = view
        self.router = router
        self.isOnboarding = isOnboarding
        self.delegate = delegate
    }
}

// MARK: AgeDataSource
extension AgePresenter: AgeDataSource {}

// MARK: AgeInputDelegate
extension AgePresenter: AgeInputDelegate {
    func datePickerDidChangeDate(_ date: Date) {
        let sign = ZodiacSignManager.shared.signFrom(date)
        selectedZodiacSign = sign
        selectedDate = date
        view.updateZodiacLabelWith(zodiacSign: sign)
    }
    
    func nextButtonDidTap() {
        guard
            let zodiacSign = selectedZodiacSign,
            let date = selectedDate
        else { return }
        
        AppPreferences.shared.zodiacSign = zodiacSign
        AppPreferences.shared.birthDate = date
        
        if isOnboarding {
            AppPreferences.shared.isOnboardingViewed = true
            router.toNotificationsScreen()
        } else {
            delegate?.update()
            router.toBack()
        }
    }
}

// MARK: BaseViewDelegate
extension AgePresenter: BaseViewDelegate {
    func viewDidLoad() {
        if let selectedDate = AppPreferences.shared.birthDate, isOnboarding == false {
            let sign = ZodiacSignManager.shared.signFrom(selectedDate)
            selectedZodiacSign = sign
            view.updateZodiacLabelWith(zodiacSign: sign)
        }
        
        view.updateNextButtonWith(title: isOnboarding ? R.string.localizable.commonNext() : R.string.localizable.commonSave())
    }
}
