//
//  AgeRouter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class AgeRouter: Router<AgeViewController> {
    func toNotificationsScreen() {
        let controller = NotificationsConfigurator.configure()
        viewController?.navigationController?.setViewControllers([controller], animated: true)
    }
    
    func toBack() {
        viewController?.dismiss(animated: true)
    }
}
