//
//  HoroscopeRouter.swift
//  LoveCompatibility
//
//  Created by Kirill on 28.02.2021.
//  Copyright (c) 2021 kyrylyermak. All rights reserved.
//

import UIKit

final class HoroscopeRouter: Router<HoroscopeViewController> {
    func toSellingScreen() {
        let controller = SellingConfigurator.configure()
        viewController?.present(controller, animated: true, completion: nil)
    }
}
