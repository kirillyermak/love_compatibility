//
//  HoroscopePresenter.swift
//  LoveCompatibility
//
//  Created by Kirill on 28.02.2021.
//  Copyright (c) 2021 kyrylyermak. All rights reserved.
//

import UIKit

protocol HoroscopeView: class {
    func updateHoroscope(text: String, title: String, isLocked: Bool)
    func updateZodiacLabelWith(zodiacSign: ZodiacSign)
    func setInternet(enabled: Bool)
}

final class HoroscopePresenter {
    private unowned let view: HoroscopeView
    private let router: HoroscopeRouter
    
    private var selectedSign = AppPreferences.shared.zodiacSign
    private var segmentedControlValue: DateSegmentedControlValue = .today
    private var selectedHoroscope: HoroscopeType = .base
    
    init(view: HoroscopeView, router: HoroscopeRouter) {
        self.view = view
        self.router = router
    }
}

// MARK: HoroscopeDataSource
extension HoroscopePresenter: HoroscopeDataSource {
    var userSign: ZodiacSign? {
        return selectedSign
    }
    
    func getZodiacSign(point: CGFloat) -> ZodiacSign? {
        return ZodiacSignManager.shared.signFrom(point: point, type: .bottom)
    }
}

// MARK: HoroscopeInputDelegate
extension HoroscopePresenter: HoroscopeInputDelegate {
    func unlockAllButtonDidTap() {
        router.toSellingScreen()
    }
    
    func segmentedControlValueChanged(_ value: DateSegmentedControlValue) {
        segmentedControlValue = value
        updateHotoscopeLabel()
    }
    
    func setSign(_ sign: ZodiacSign) {
        selectedSign = sign
        updateHotoscopeLabel()
    }
    
    func selectedHoroscope(_ horoscope: HoroscopeType) {
        selectedHoroscope = horoscope
        updateHotoscopeLabel()
    }
}

// MARK: BaseViewDelegate
extension HoroscopePresenter: BaseViewDelegate {
    func viewDidLoad() {
        guard let sign = selectedSign else { return }
        updateHotoscopeLabel()
        view.updateZodiacLabelWith(zodiacSign: sign)
    }
    
    func viewWillAppear() {
        view.setInternet(enabled: Reachability.isReachable())
    }
}

// MARK: Private functions
private extension HoroscopePresenter {
    private func updateHotoscopeLabel() {
        guard let sign = selectedSign else { return }
        
        var text = ""
        var horoscope: Horoscope?
        
        var isPremiumHoroscope = false
        switch selectedHoroscope {
        case .base:
            isPremiumHoroscope = false
            horoscope = HoroscopeManager.shared.baseHoro[sign.id] ?? nil
        case .erotic:
            isPremiumHoroscope = true
            horoscope = HoroscopeManager.shared.eroticHoro[sign.id] ?? nil
        case .anti:
            isPremiumHoroscope = false
            horoscope = HoroscopeManager.shared.antiHoro[sign.id] ?? nil
        case .business:
            isPremiumHoroscope = true
            horoscope = HoroscopeManager.shared.businessHoro[sign.id] ?? nil
        case .health:
            isPremiumHoroscope = true
            horoscope = HoroscopeManager.shared.healthHoro[sign.id] ?? nil
        case .cooking:
            isPremiumHoroscope = false
            horoscope = HoroscopeManager.shared.cookingHoro[sign.id] ?? nil
        case .love:
            isPremiumHoroscope = true
            horoscope = HoroscopeManager.shared.loveHoro[sign.id] ?? nil
        case .mobile:
            isPremiumHoroscope = false
            horoscope = HoroscopeManager.shared.mobileHoro[sign.id] ?? nil
        }
        
        var isSegmentLocked = false
        switch segmentedControlValue {
        case .yesterday:
            isSegmentLocked = false
            text += horoscope?.yesterday ?? R.string.localizable.horoscopeOopsSomethingWentWrong()
        case .today:
            isSegmentLocked = false
            text += horoscope?.today ?? R.string.localizable.horoscopeOopsSomethingWentWrong()
        case .tomorrow:
            text += horoscope?.tomorrow ?? R.string.localizable.horoscopeOopsSomethingWentWrong()
            isSegmentLocked = PurchaseManager.shared.isPremium == false
        }
        
        let isLocked = PurchaseManager.shared.isPremium == false && (isSegmentLocked || isPremiumHoroscope)
        
        if isLocked {
            text = String(text.prefix(50)) + "..."
        }
        
        view.updateHoroscope(text: text,
                             title: selectedHoroscope.title(),
                             isLocked: isLocked)
    }
}

enum DateSegmentedControlValue: Int {
    case yesterday
    case today
    case tomorrow
}
