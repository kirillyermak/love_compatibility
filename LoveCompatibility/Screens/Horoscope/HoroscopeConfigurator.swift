//
//  HoroscopeConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill on 28.02.2021.
//  Copyright (c) 2021 kyrylyermak. All rights reserved.
//

import UIKit

final class HoroscopeConfigurator {
    static func configure() -> UIViewController {
        let viewController = HoroscopeViewController()
        let router = HoroscopeRouter(viewController)
        let presenter = HoroscopePresenter(view: viewController,
                                           router: router)
        viewController.dataSource = presenter
        viewController.input = presenter
        
        return viewController
    }
}
