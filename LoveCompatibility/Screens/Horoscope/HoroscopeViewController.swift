//
//  HoroscopeViewController.swift
//  LoveCompatibility
//
//  Created by Kirill on 28.02.2021.
//  Copyright (c) 2021 kyrylyermak. All rights reserved.
//

import UIKit
import PinLayout
import BetterSegmentedControl
import Haptico
import Lottie

protocol HoroscopeDataSource: class {
    var userSign: ZodiacSign? { get }
    
    func getZodiacSign(point: CGFloat) -> ZodiacSign?
}

protocol HoroscopeInputDelegate: BaseViewDelegate {
    func unlockAllButtonDidTap()
    func segmentedControlValueChanged(_ value: DateSegmentedControlValue)
    func selectedHoroscope(_ horoscope: HoroscopeType)
    func setSign(_ sign: ZodiacSign)
}

final class HoroscopeViewController: BaseViewController {
    // MARK: Properties
    var dataSource: HoroscopeDataSource?
    var input: HoroscopeInputDelegate?
    
    private var touchBeganAt: CGPoint?
    
    private var horoscopes: [HoroscopeType] = [
        .base,
        .business,
        .health,
        .anti,
        .cooking,
        .love,
        .erotic,
        .mobile
    ]
    
    // MARK: UI
    private let zodiacSignCircleImageView = UIImageView()
    private let zodiacView = UIView()
    private let zodiacSignLabel = UILabel()
    private let dateSegmentedControl = BetterSegmentedControl()
    private let horoscopeSegmentedControl = BetterSegmentedControl()
    private let horoscopeLabel = UILabel()
    private let horoscopeTitleLabel = UILabel()
    private let unlockButton = UIButton()
    private let animationView = AnimationView()
    private let swipeView = UIView()
    private let noInternetView = UIView()
    private let noInternetLabel = UILabel()
    private let noInternetAniamationView = AnimationView()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        input?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        input?.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        AppOpenAdManager.shared.presentAd(from: self)
    }
    
    // MARK: Setup
    override func addSubviews() {
        view.addSubview(zodiacSignLabel)
        view.addSubview(zodiacSignCircleImageView)
        view.addSubview(zodiacView)
        view.addSubview(dateSegmentedControl)
        view.addSubview(horoscopeSegmentedControl)
        view.addSubview(horoscopeLabel)
        view.addSubview(horoscopeTitleLabel)
        view.addSubview(animationView)
        view.addSubview(swipeView)
        view.addSubview(unlockButton)
        noInternetView.addSubview(noInternetLabel)
        noInternetView.addSubview(noInternetAniamationView)
        view.addSubview(noInternetView)
    }
    
    override func setupLayout() {
        if DeviceType.isX {
            zodiacSignCircleImageView.pin
                .bottom((view.pin.safeArea.bottom + view.frame.height * 0.2) * -1)
                .width(150%)
                .hCenter()
                .aspectRatio(1/1)
        } else {
            zodiacSignCircleImageView.pin
                .bottom((view.pin.safeArea.bottom + view.frame.height * 0.4) * -1)
                .width(150%)
                .hCenter()
                .aspectRatio(1/1)
        }
        
        zodiacView.pin
            .size(of: zodiacSignCircleImageView)
            .center(to: zodiacSignCircleImageView.anchor.center)
        
        zodiacSignLabel.pin
            .top(10 + view.pin.safeArea.top)
            .horizontally(35)
            .sizeToFit(.width)
        
        dateSegmentedControl.pin
            .height(35)
            .below(of: zodiacSignLabel)
            .marginTop(15)
            .horizontally(35)
        
        horoscopeSegmentedControl.pin
            .below(of: dateSegmentedControl)
            .marginTop(15)
            .height(35)
            .horizontally(35)
        
        horoscopeTitleLabel.pin
            .below(of: horoscopeSegmentedControl)
            .marginTop(8)
            .horizontally(35)
            .sizeToFit(.width)
        
        horoscopeLabel.pin
            .verticallyBetween(horoscopeTitleLabel, and: zodiacSignCircleImageView)
            .horizontally(20)
        
        unlockButton.pin
            .height(34)
            .horizontally(30)
            .above(of: zodiacSignCircleImageView)
            .marginBottom(30)
        
        swipeView.pin
            .horizontally()
            .verticallyBetween(horoscopeTitleLabel, and: zodiacSignCircleImageView)
        
        animationView.pin
            .width(unlockButton.frame.width * 2)
            .height(unlockButton.frame.height * 3)
            .center(to: unlockButton.anchor.center)
        
        noInternetView.pin
            .all()
        
        noInternetAniamationView.pin
            .all()
        
        noInternetLabel.pin
            .horizontally(30)
            .bottom((tabBarController?.tabBar.frame.height ?? 40) + 10)
            .sizeToFit(.width)
    }
    
    override func setupAppearance() {
        view.backgroundColor = R.color.background()
        
        dateSegmentedControl.segments = LabelSegment.segments(
            withTitles: [
                R.string.localizable.horoscopeYesterday(),
                R.string.localizable.horoscopeToday(),
                R.string.localizable.horoscopeTomorrow()
            ],
            normalFont: R.font.sfProDisplayLight(size: 13),
            normalTextColor: R.color.sub_text(),
            selectedFont: R.font.sfProDisplayMedium(size: 13),
            selectedTextColor: R.color.main_text()
        )
        
        let secondaryBackground = R.color.secondary_background()
        let secondaryBackgroundLight = R.color.secondary_background_light()
        
        dateSegmentedControl.backgroundColor = secondaryBackground
        dateSegmentedControl.indicatorView.backgroundColor = secondaryBackgroundLight
        let cornerRadius: CGFloat = 13
        dateSegmentedControl.indicatorView.layer.cornerRadius = cornerRadius - 1
        dateSegmentedControl.layer.cornerRadius = cornerRadius
        dateSegmentedControl.setIndex(1)
        
        let titles = horoscopes.map { $0.emoji() }
        
        horoscopeSegmentedControl.segments = LabelSegment.segments(
            withTitles: titles,
            normalFont: R.font.sfProDisplayLight(size: 20),
            normalTextColor: R.color.sub_text(),
            selectedFont: R.font.sfProDisplayLight(size: 20),
            selectedTextColor: R.color.main_text()
        )
        
        horoscopeSegmentedControl.backgroundColor = secondaryBackground
        horoscopeSegmentedControl.indicatorView.backgroundColor = secondaryBackgroundLight
        horoscopeSegmentedControl.indicatorView.layer.cornerRadius = cornerRadius - 1
        horoscopeSegmentedControl.layer.cornerRadius = cornerRadius
        
        zodiacSignCircleImageView.image = R.image.zodiac_circle()
        
        zodiacSignLabel.textAlignment = .center
        zodiacSignLabel.font = R.font.sfProDisplayBold(size: 40)
        zodiacSignLabel.textColor = R.color.blue_text()
        
        horoscopeLabel.textAlignment = .center
        horoscopeLabel.numberOfLines = 0
        horoscopeLabel.textColor = R.color.main_text()
        horoscopeLabel.font = R.font.sfProDisplayRegular(size: 16)
        
        horoscopeTitleLabel.textAlignment = .center
        horoscopeTitleLabel.font = R.font.sfProDisplayLight(size: 13)
        horoscopeTitleLabel.textColor = R.color.orange_text()
        
        unlockButton.titleLabel?.font = R.font.sfProDisplayBold(size: 20)
        let gender = AppPreferences.shared.gender
        let title = "\(gender == .female ? " 👸🏽" : " 🤴🏽") \(R.string.localizable.compatibilityInfoUnlockAll().uppercased())"
        unlockButton.setTitle(title, for: .normal)
        unlockButton.setTitleColor(R.color.dark_main_text(), for: .normal)
        
        animationView.animation = Animation.greenButton
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.backgroundBehavior = .pauseAndRestore
        animationView.play()
        
        noInternetView.backgroundColor = R.color.background()
        
        noInternetAniamationView.animation = Animation.noInternet
        noInternetAniamationView.contentMode = .scaleAspectFit
        noInternetAniamationView.loopMode = .loop
        noInternetAniamationView.backgroundBehavior = .pauseAndRestore
        noInternetAniamationView.play()
        
        noInternetLabel.font = R.font.sfProDisplayMedium(size: 25)
        noInternetLabel.textAlignment = .center
        noInternetLabel.text = R.string.localizable.horoscopeInternetConnection()
        noInternetLabel.numberOfLines = 0
    }
    
    override func setupActions() {
        dateSegmentedControl.addTarget(self, action: #selector(dateSegmentedControlValueChanged), for: .valueChanged)
        horoscopeSegmentedControl.addTarget(self, action: #selector(horoscopeSegmentedControlValueChanged), for: .valueChanged)
        unlockButton.addTarget(self, action: #selector(unlockButtonAction), for: .touchUpInside)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeLeft.direction = .left
        swipeView.addGestureRecognizer(swipeLeft)

        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeRight.direction = .right
        swipeView.addGestureRecognizer(swipeRight)
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        zodiacView.addGestureRecognizer(gestureRecognizer)
    }
}

// MARK: Button Actions
private extension HoroscopeViewController {
    @objc private  func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        updateZodiacSignLabels()
        
        if gestureRecognizer.state == .ended {
            let rotation = atan2(zodiacSignCircleImageView.transform.b, zodiacSignCircleImageView.transform.a)
            
            if let sign = dataSource?.getZodiacSign(point: radiansToDegrees(rotation)) {
                UIView.animate(withDuration: 0.3) { [weak self] in
                    guard let `self` = self else { return }
                    self.zodiacSignCircleImageView.transform = .identity
                    let transform = self.zodiacSignCircleImageView.transform.rotated(by: self.degreesToRadians(sign.bottomAngle))
                    self.zodiacSignCircleImageView.transform = transform
                }
            }
        }
        
        guard gestureRecognizer.state == .began || gestureRecognizer.state == .changed else { return }
        
        let point = gestureRecognizer.location(ofTouch: 0, in: zodiacView)
        
        if gestureRecognizer.state == .began {
            touchBeganAt = point
        }
        
        if gestureRecognizer.state == .changed {
            let diff = (touchBeganAt?.x ?? 0) - point.x
            zodiacSignCircleImageView.transform = zodiacSignCircleImageView.transform.rotated(by: degreesToRadians(diff) * -1 / 4)
            touchBeganAt = gestureRecognizer.location(ofTouch: 0, in: zodiacView)
        }
    }
    
    @objc private func handleSwipe(gesture: UISwipeGestureRecognizer) {
        let horoscopesCount = horoscopes.count
        
        var index = horoscopeSegmentedControl.index
        
        if gesture.direction == .right {
            index -= 1
        } else if gesture.direction == .left {
            index += 1
        }
        
        if index <= horoscopesCount - 1 && index >= 0 {
            horoscopeSegmentedControl.setIndex(index)
            let horoscope = horoscopes[index]
            input?.selectedHoroscope(horoscope)
            Haptico.shared().generate(.light)
        }
    }
}

// MARK: HoroscopeView
extension HoroscopeViewController: HoroscopeView {
    func updateHoroscope(text: String, title: String, isLocked: Bool) {
        horoscopeLabel.text = text
        horoscopeLabel.sizeToFit()
        horoscopeTitleLabel.text = title
        
        if isLocked {
            horoscopeLabel.alpha = 0.2
            animationView.isHidden = false
            unlockButton.isHidden = false
        } else {
            horoscopeLabel.alpha = 1
            animationView.isHidden = true
            unlockButton.isHidden = true
        }
    }
    
    func updateZodiacLabelWith(zodiacSign: ZodiacSign) {
        zodiacSignLabel.text = zodiacSign.title
        zodiacSignLabel.sizeToFit()
        
        UIView.animate(withDuration: 0.4) { [weak self] in
            guard let `self` = self else { return }
            self.zodiacSignCircleImageView.transform = .identity
            let transform = self.zodiacSignCircleImageView.transform.rotated(by: self.degreesToRadians(zodiacSign.bottomAngle))
            self.zodiacSignCircleImageView.transform = transform
        }
    }
    
    func setInternet(enabled: Bool) {
        noInternetView.isHidden = enabled
    }
}

// MARK: Private functions
private extension HoroscopeViewController {
    @objc private func dateSegmentedControlValueChanged() {
        guard let value = DateSegmentedControlValue(rawValue: dateSegmentedControl.index) else { return }
        input?.segmentedControlValueChanged(value)
        Haptico.shared().generate(.light)
    }
    
    @objc private func horoscopeSegmentedControlValueChanged() {
        let horoscope = horoscopes[horoscopeSegmentedControl.index]
        input?.selectedHoroscope(horoscope)
        
        dateSegmentedControl.setIndex(1)
        dateSegmentedControlValueChanged()
        Haptico.shared().generate(.light)
    }
    
    @objc private func unlockButtonAction() {
        input?.unlockAllButtonDidTap()
    }
    
    private func degreesToRadians(_ deg: CGFloat) -> CGFloat {
        return deg * CGFloat.pi / 180
    }
    
    private func radiansToDegrees(_ rad: CGFloat) -> CGFloat {
        return rad / CGFloat.pi * 180
    }
    
    private func updateZodiacSignLabels() {
        let rotation = atan2(zodiacSignCircleImageView.transform.b, zodiacSignCircleImageView.transform.a)
        
        if let sign = dataSource?.getZodiacSign(point: radiansToDegrees(rotation)),
           dataSource?.userSign != sign {
            
            if dataSource?.userSign != nil {
                Haptico.shared().generate(.light)
            }
            
            zodiacSignLabel.text = sign.title
            input?.setSign(sign)
        }
    }
}
