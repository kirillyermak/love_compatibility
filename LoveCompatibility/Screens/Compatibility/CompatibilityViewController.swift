//
//  CompatibilityViewController.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import PinLayout
import Haptico
import Lottie
import YLProgressBar

protocol CompatibilityDataSource: class {
    var userSign: ZodiacSign? { get }
    var partnerSign: ZodiacSign? { get }
    
    func getZodiacSign(point: CGFloat, type: WheelType) -> ZodiacSign?
}

protocol CompatibilityInputDelegate: BaseViewDelegate, PreResultDelegate {
    func showCompatibilityButtonDidTap()
    func setSign(type: WheelType, sign: ZodiacSign)
}

final class CompatibilityViewController: BaseViewController {
    // MARK: Properties
    var dataSource: CompatibilityDataSource?
    var input: CompatibilityInputDelegate?
    
    private var touchBeganAt: CGPoint?
    
    // MARK: UI
    private let backAnimationView = AnimationView(name: "stars")
    private let youLabel = UILabel()
    private let leftZodiacView = UIView()
    private let leftZodiacSignLabel = UILabel()
    private let leftZodiacCircleImageView = UIImageView()
    private let partnerLabel = UILabel()
    private let rightZodiacView = UIView()
    private let rightZodiacSignLabel = UILabel()
    private let rightZodiacCircleImageView = UIImageView()
    private let progressLabel = UILabel()
    private let progressBar = YLProgressBar()
    private let showCompatibilityAnimationView = AnimationView()
    private let showCompatibilityButton = UIButton()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        input?.viewDidLoad()
        update()
    }
    
    func update() {
        leftZodiacCircleImageView.transform = .identity
        rightZodiacCircleImageView.transform = .identity
        
        if let sign = AppPreferences.shared.zodiacSign {
            setDefaultZodiacSign(sign)
        }
        
        updateZodiacSignLabels()
        
        let femaleGender = "👩"
        let maleGender = "👨"
        youLabel.text = R.string.localizable.compatibilityYou() + " " + (AppPreferences.shared.gender == .female ? femaleGender : maleGender)
        partnerLabel.text = (AppPreferences.shared.gender == .female ? maleGender : femaleGender) + " " + R.string.localizable.compatibilityPartner()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    // MARK: Setup
    override func addSubviews() {
        view.addSubview(backAnimationView)
        view.addSubview(youLabel)
        view.addSubview(leftZodiacSignLabel)
        view.addSubview(leftZodiacCircleImageView)
        view.addSubview(leftZodiacView)
        view.addSubview(partnerLabel)
        view.addSubview(rightZodiacSignLabel)
        view.addSubview(rightZodiacCircleImageView)
        view.addSubview(rightZodiacView)
        view.addSubview(showCompatibilityAnimationView)
        view.addSubview(showCompatibilityButton)
        view.addSubview(progressBar)
        view.addSubview(progressLabel)
    }
    
    override func setupLayout() {
        backAnimationView.pin
            .all()
        
        youLabel.pin
            .top(20 + view.pin.safeArea.top)
            .left(20)
            .width(35%)
            .sizeToFit(.width)
        
        leftZodiacSignLabel.pin
            .below(of: youLabel)
            .marginTop(5)
            .left(to: youLabel.edge.left)
            .width(35%)
            .sizeToFit(.width)
        
        leftZodiacCircleImageView.pin
            .width(132%)
            .left(-90%)
            .vCenter(-2%)
            .aspectRatio(1)
        
        leftZodiacView.pin
            .size(of: leftZodiacCircleImageView)
            .center(to: leftZodiacCircleImageView.anchor.center)
        
        partnerLabel.pin
            .top(to: youLabel.edge.top)
            .right(20)
            .width(35%)
            .sizeToFit(.width)
        
        rightZodiacSignLabel.pin
            .below(of: partnerLabel)
            .marginTop(5)
            .right(to: partnerLabel.edge.right)
            .width(35%)
            .sizeToFit(.width)
        
        rightZodiacCircleImageView.pin
            .size(of: leftZodiacCircleImageView)
            .right(-90%)
            .vCenter(-2%)
        
        rightZodiacView.pin
            .size(of: rightZodiacCircleImageView)
            .center(to: rightZodiacCircleImageView.anchor.center)
        
        showCompatibilityButton.pin
            .horizontally(30)
            .height(47)
            .bottom(view.pin.safeArea.bottom + 20)
        
        showCompatibilityAnimationView.pin
            .center(to: showCompatibilityButton.anchor.center)
            .width(showCompatibilityButton.frame.width * 2)
            .height(showCompatibilityButton.frame.height * 3)
        
        progressBar.pin
            .above(of: showCompatibilityButton)
            .marginBottom(20)
            .width(60%)
            .height(13)
            .hCenter()
        
        progressLabel.pin
            .above(of: progressBar)
            .marginBottom(5)
            .width(of: showCompatibilityButton)
            .sizeToFit(.width)
            .hCenter()
    }
    
    override func setupAppearance() {
        navigationController?.navigationBar.barStyle = .black
        
        view.backgroundColor = R.color.dark_background()
        
        let lightColor = R.color.light_main_text()
        
        youLabel.textColor = lightColor
        youLabel.font = R.font.sfProDisplayLight(size: 15)
        
        leftZodiacCircleImageView.image = R.image.zodiac_circle()
        
        let signFont = R.font.sfProDisplayMedium(size: 25)
        
        leftZodiacSignLabel.font = signFont
        leftZodiacSignLabel.textColor = lightColor
        
        partnerLabel.textAlignment = .right
        partnerLabel.textColor = lightColor
        partnerLabel.font = R.font.sfProDisplayLight(size: 15)
        
        rightZodiacCircleImageView.image = R.image.zodiac_circle()
        
        rightZodiacSignLabel.textAlignment = .right
        rightZodiacSignLabel.font = signFont
        rightZodiacSignLabel.textColor = lightColor
        
        backAnimationView.contentMode = .scaleAspectFit
        backAnimationView.loopMode = .loop
        backAnimationView.backgroundBehavior = .pauseAndRestore
        backAnimationView.play()
        
        showCompatibilityAnimationView.contentMode = .scaleAspectFit
        showCompatibilityAnimationView.loopMode = .loop
        showCompatibilityAnimationView.backgroundBehavior = .pauseAndRestore
        showCompatibilityAnimationView.play()
        
        progressBar.progressTintColor = R.color.button_green()!
        progressBar.trackTintColor = R.color.main_text()!
        progressBar.progressBarInset = 0
        progressBar.cornerRadius = 12
        progressBar.setProgress(0, animated: false)
        progressBar.hideStripes = true
        progressBar.hideGloss = true
        progressBar.isStripesAnimated = true
        progressBar.hideInnerWhiteShadow = true
        progressBar.progressStretch = false
        progressBar.alpha = 0
        
        progressLabel.textAlignment = .center
        progressLabel.font = R.font.sfProDisplayThinItalic(size: 15)
        progressLabel.alpha = 0
        progressLabel.textColor = lightColor
        
        showCompatibilityButton.titleLabel?.font = R.font.sfProDisplayBold(size: 18)
    }
    
    override func setupActions() {
        showCompatibilityButton.addTarget(self, action: #selector(showCompatibilityButtonAction), for: .touchUpInside)
        
        let leftGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        leftZodiacView.addGestureRecognizer(leftGestureRecognizer)
        
        let rightGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        rightZodiacView.addGestureRecognizer(rightGestureRecognizer)
    }
}

// MARK: Button Actions
private extension CompatibilityViewController {
    @objc private func showCompatibilityButtonAction() {
        input?.showCompatibilityButtonDidTap()
        Haptico.shared().generate(.success)
    }
    
    @IBAction func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        updateZodiacSignLabels()
        
        if gestureRecognizer.state == .ended {
            leftZodiacView.isUserInteractionEnabled = true
            rightZodiacView.isUserInteractionEnabled = true
            
            let leftRotation = atan2(leftZodiacCircleImageView.transform.b, leftZodiacCircleImageView.transform.a)
            let rightRotation = atan2(rightZodiacCircleImageView.transform.b, rightZodiacCircleImageView.transform.a)
            
            if let sign = dataSource?.getZodiacSign(point: radiansToDegrees(leftRotation), type: .left) {
                UIView.animate(withDuration: 0.3) { [weak self] in
                    guard let `self` = self else { return }
                    self.leftZodiacCircleImageView.transform = .identity
                    let transform = self.leftZodiacCircleImageView.transform.rotated(by: self.degreesToRadians(sign.leftAngle))
                    self.leftZodiacCircleImageView.transform = transform
                }
            }
            
            if let sign = dataSource?.getZodiacSign(point: radiansToDegrees(rightRotation), type: .right) {
                UIView.animate(withDuration: 0.3) { [weak self] in
                    guard let `self` = self else { return }
                    self.rightZodiacCircleImageView.transform = .identity
                    let transform = self.rightZodiacCircleImageView.transform.rotated(by: self.degreesToRadians(sign.rightAngle))
                    self.rightZodiacCircleImageView.transform = transform
                }
            }
        }
        
        guard gestureRecognizer.state == .began || gestureRecognizer.state == .changed else { return }
        let type: WheelType = gestureRecognizer.view == leftZodiacView ? .left : .right
        let view = type == .left ? leftZodiacView : rightZodiacView
        let image = type == .left ? leftZodiacCircleImageView : rightZodiacCircleImageView
        
        let point = gestureRecognizer.location(ofTouch: 0, in: view)
        
        if gestureRecognizer.state == .began {
            touchBeganAt = point
            
            if type == .left {
                rightZodiacView.isUserInteractionEnabled = false
            } else {
                leftZodiacView.isUserInteractionEnabled = false
            }
        }
        
        if gestureRecognizer.state == .changed {
            let diff = (touchBeganAt?.y ?? 0) - point.y
            image.transform = image.transform.rotated(by: degreesToRadians(diff) * (type == .left ? -1 : 1) / 4)
            touchBeganAt = gestureRecognizer.location(ofTouch: 0, in: view)
        }
    }
}

// MARK: CompatibilityView
extension CompatibilityViewController: CompatibilityView {
    func setProgress(progress: CGFloat, text: String) {
        progressBar.setProgress(progress, animated: true)
        progressLabel.text = text
        progressLabel.sizeToFit()
    }
    
    func setViewStyle(_ style: CompatibilityPresenter.Style) {
        switch style {
        case .searching:
            showCompatibilityButton.setTitleColor(R.color.dark_main_text(), for: .normal)
            showCompatibilityButton.setTitle(R.string.localizable.compatibilityLoading(), for: .normal)
            showCompatibilityButton.isEnabled = false
            leftZodiacView.isUserInteractionEnabled = false
            rightZodiacView.isUserInteractionEnabled = false
            
            showCompatibilityAnimationView.animation = Animation.coralButton
            showCompatibilityAnimationView.play()
            
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.progressBar.alpha = 1
                self?.progressLabel.alpha = 1
            }
        case .found:
            showCompatibilityButton.setTitleColor(R.color.dark_main_text(), for: .normal)
            showCompatibilityButton.setTitle(R.string.localizable.compatibilityLetMeKnow(), for: .normal)
            showCompatibilityButton.isEnabled = true
            leftZodiacView.isUserInteractionEnabled = true
            rightZodiacView.isUserInteractionEnabled = true
            
            Haptico.shared().generate(.success)
            
            showCompatibilityAnimationView.animation = Animation.greenButton
            showCompatibilityAnimationView.play()
            
            UIView.animateKeyframes(withDuration: 0.3, delay: 1) { [weak self] in
                self?.progressBar.alpha = 0
                self?.progressLabel.alpha = 0
            }
        case .active:
            showCompatibilityButton.setTitleColor(R.color.light_main_text(), for: .normal)
            showCompatibilityButton.setTitle(R.string.localizable.compatibilityСompatible(), for: .normal)
            showCompatibilityButton.isEnabled = true
            leftZodiacView.isUserInteractionEnabled = true
            rightZodiacView.isUserInteractionEnabled = true
            progressBar.setProgress(0, animated: false)
            
            showCompatibilityAnimationView.animation = Animation.purpleButton
            showCompatibilityAnimationView.play()
            
            UIView.animate(withDuration: 0.3) { [weak self] in
                self?.progressBar.alpha = 0
                self?.progressLabel.alpha = 0
            }
        }
    }
}

// MARK: Private functions
private extension CompatibilityViewController {
    private func updateZodiacSignLabels() {
        if let sign = dataSource?.getZodiacSign(point: radiansToDegrees(rotation(of: .left)), type: .left),
           dataSource?.userSign != sign {
            
            if dataSource?.userSign != nil {
                Haptico.shared().generate(.medium)
            }
            
            leftZodiacSignLabel.text = sign.title
            input?.setSign(type: .left, sign: sign)
        }
        
        if let sign = dataSource?.getZodiacSign(point: radiansToDegrees(rotation(of: .right)), type: .right),
           dataSource?.partnerSign != sign {
            
            if dataSource?.partnerSign != nil {
                Haptico.shared().generate(.medium)
            }
            
            rightZodiacSignLabel.text = sign.title
            input?.setSign(type: .right, sign: sign)
        }
    }
    
    private func setDefaultZodiacSign(_ sign: ZodiacSign) {
        let transform = leftZodiacCircleImageView.transform.rotated(by: degreesToRadians(sign.leftAngle))
        leftZodiacCircleImageView.transform = transform
    }
    
    private func rotation(of wheel: WheelType) -> CGFloat {
        if wheel == .left {
            return atan2(leftZodiacCircleImageView.transform.b, leftZodiacCircleImageView.transform.a)
        } else {
            return atan2(rightZodiacCircleImageView.transform.b, rightZodiacCircleImageView.transform.a)
        }
    }
    
    private func degreesToRadians(_ deg: CGFloat) -> CGFloat {
        return deg * CGFloat.pi / 180
    }
    
    private func radiansToDegrees(_ rad: CGFloat) -> CGFloat {
        return rad / CGFloat.pi * 180
    }
}
