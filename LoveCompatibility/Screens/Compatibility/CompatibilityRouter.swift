//
//  CompatibilityRouter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class CompatibilityRouter: Router<CompatibilityViewController> {
    private let transition = ScaleTransition()
    
    func toCompatibilityInfoScreen(pair: ZodiacPair) {
        let controller = CompatibilityInfoConfigurator.configure(pair: pair)
        viewController?.present(controller, animated: true, completion: nil)
    }
    
    func toSeliingScreen() {
        let controller = SellingConfigurator.configure()
        viewController?.present(controller, animated: true, completion: nil)
    }
    
    func toPreResultPopUp() {
        guard let delegate = viewController?.input else { return }
        
        let controller = PreResultConfigurator.configure(delegate: delegate)
        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = transition
        viewController?.present(controller, animated: true, completion: nil)
    }
    
    func toAd(with helper: RewardedAdHelper) {
        guard let controller = viewController else { return }
        helper.createAndShowAd(from: controller)
    }
}
