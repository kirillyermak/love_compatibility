//
//  CompatibilityPresenter.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation
import UIKit

protocol CompatibilityView: class {
    func setProgress(progress: CGFloat, text: String)
    func setViewStyle(_ style: CompatibilityPresenter.Style)
}

final class CompatibilityPresenter {
    private unowned let view: CompatibilityView
    private let router: CompatibilityRouter
    
    private var userZodiacSign: ZodiacSign?
    private var partnerZodiacSign: ZodiacSign?
    private var progress: CGFloat = 0
    private var loadingIndex = 0
    private let rewardedAdHelper = RewardedAdHelper()
    private var loadingTimer: Timer?
    private var viewState: Style! {
        didSet {
            view.setViewStyle(viewState)
        }
    }
    
    init(view: CompatibilityView, router: CompatibilityRouter) {
        self.view = view
        self.router = router
    }
}

// MARK: - CompatibilityDataSource
extension CompatibilityPresenter: CompatibilityDataSource {
    var userSign: ZodiacSign? {
        return userZodiacSign
    }
    
    var partnerSign: ZodiacSign? {
        return partnerZodiacSign
    }
    
    func getZodiacSign(point: CGFloat, type: WheelType) -> ZodiacSign? {
        return ZodiacSignManager.shared.signFrom(point: point, type: type)
    }
}

// MARK: - CompatibilityInputDelegate
extension CompatibilityPresenter: CompatibilityInputDelegate {
    func showCompatibilityButtonDidTap() {
        let isPremium = PurchaseManager.shared.isPremium
        
        if isPremium {
            moveToCompatibilityInfoScreen()
        } else {
            if viewState == .found {
                router.toPreResultPopUp()
            } else {
                startLoading()
            }
        }
    }
    
    func setSign(type: WheelType, sign: ZodiacSign) {
        if viewState != .active {
            viewState = .active
        }
        
        if type == .left {
            userZodiacSign = sign
        } else {
            partnerZodiacSign = sign
        }
        
        loadingTimer?.invalidate()
        loadingTimer = nil
    }
}

// MARK: - PreResultDelegate
extension CompatibilityPresenter: PreResultDelegate {
    func watchAdButtonDidTap() {
        router.toAd(with: rewardedAdHelper)
    }
    
    func activateProButtonDidTap() {
        router.toSeliingScreen()
    }
}

// MARK: - BaseViewDelegate
extension CompatibilityPresenter: BaseViewDelegate {
    func viewDidLoad() {
        viewState = .active
        rewardedAdHelper.delegate = self
    }
}

// MARK: - Private functions
private extension CompatibilityPresenter {
    private func moveToCompatibilityInfoScreen() {
        guard
            let userSign = userZodiacSign,
            let partnerSign = partnerZodiacSign
        else { return }
        
        router.toCompatibilityInfoScreen(pair: ZodiacPair(user: userSign, partner: partnerSign))
    }
    
    private func startLoading() {
        progress = 0
        viewState = .searching
        
        loadingTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { [weak self] timer in
            self?.progress += 1
            
            guard let progress = self?.progress else { return }
            
            self?.view.setProgress(progress: progress / 100, text: "")
            
            if progress.truncatingRemainder(dividingBy: 20) == 0 {
                self?.loadingIndex += 1
            }
            
            if progress >= 100 {
                self?.view.setProgress(progress: 1, text: "Готово")
                self?.loadingIndex = 0
                self?.viewState = .found
                timer.invalidate()
            }
        }
    }
}

// MARK: RewardedAdHelperDelegate
extension CompatibilityPresenter: RewardedAdHelperDelegate {
    func rewardedAdHelperDidRecieveReward() {
        moveToCompatibilityInfoScreen()
    }
}

// MARK: Enumerations
extension CompatibilityPresenter {
    enum Style {
        case found
        case searching
        case active
    }
}
