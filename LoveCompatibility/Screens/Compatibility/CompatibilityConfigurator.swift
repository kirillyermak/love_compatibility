//
//  CompatibilityConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class CompatibilityConfigurator {
    static func configure() -> UIViewController {
        let viewController = CompatibilityViewController()
        let router = CompatibilityRouter(viewController)
        let presenter = CompatibilityPresenter(view: viewController,
                                               router: router)
        viewController.dataSource = presenter
        viewController.input = presenter
        
        return viewController
    }
}
