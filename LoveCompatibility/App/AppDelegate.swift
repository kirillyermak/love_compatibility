//
//  AppDelegate.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 14.01.2021.
//

import UIKit
import Firebase
import GoogleMobileAds

#if DEBUG
let isDebug = true
#else
let isDebug = false
#endif

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configureWindow()
        
        _ = PurchaseManager.shared
        _ = RCManager.shared
        
        PushNotificationsManager.shared.scheduledMorningNotifications()
        HoroscopeManager.shared.requestHoroscopes()
        FirebaseApp.configure()
        
        GADMobileAds.sharedInstance().start { _ in
            _ = AppOpenAdManager.shared
        }
        
        return true
    }
}

private extension AppDelegate {
    func configureWindow() {
        let window = UIWindow()
        
        if isDebug {
            window.rootViewController = MainConfigurator.configure()
        } else {
            window.rootViewController = MainConfigurator.configure()
        }

        window.overrideUserInterfaceStyle = AppPreferences.shared.isDarkModeEnabled ? .dark : .light
        self.window = window
        self.window?.makeKeyAndVisible()
    }
}
