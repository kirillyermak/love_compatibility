//
//  MainConfigurator.swift
//  LoveCompatibility
//
//  Created by Kirill Yermak on 15.01.2021.
//

import Foundation
import UIKit

final class MainConfigurator {
    static func configure() -> UIViewController {
        if AppPreferences.shared.isOnboardingViewed == false {
            return GenderConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        } else {
            return configureMainModele()
        }
    }
    
    static func configure(_ module: Module) -> UIViewController {
        var rootViewController = UIViewController()

        switch module {
        case .onboarding:
            rootViewController = GenderConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        case .gender:
            rootViewController = GenderConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        case .age:
            rootViewController = AgeConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        case .main:
            rootViewController = configureMainModele()
        case .compatibility:
            rootViewController = CompatibilityConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        case .horoscope:
            rootViewController = HoroscopeConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        case .settings:
            rootViewController = SettingsConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        case .notifications:
            rootViewController = NotificationsConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        }
        
        return rootViewController
    }
    
    private static func configureMainModele() -> UIViewController {
        let tabBarController = UITabBarController()
        
        let horoscope = HoroscopeConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        horoscope.tabBarItem = UITabBarItem(title: R.string.localizable.horoscopeTitle(),
                                            image: R.image.horoscope_tab_bar_icon(),
                                            selectedImage: nil)
        
        let compatibility = CompatibilityConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        compatibility.tabBarItem = UITabBarItem(title: R.string.localizable.compatibilityTitle(),
                                                image: R.image.compatibility_tab_bar_icon(),
                                                selectedImage: nil)
        
        let settings = SettingsConfigurator.configure().embedInNavigationController(navigationBarIsHidden: true)
        settings.tabBarItem = UITabBarItem(title: R.string.localizable.settingsTitle(),
                                           image: R.image.settings_tab_bar_icon(),
                                           selectedImage: nil)
        
        tabBarController.viewControllers = [horoscope, compatibility, settings]
        tabBarController.tabBar.tintColor = R.color.button_purple()
        tabBarController.selectedIndex = 1
        
        AppPreferences.shared.session += 1

        return tabBarController
    }
    
    enum Module {
        case onboarding
        case gender
        case age
        case main
        case compatibility
        case horoscope
        case settings
        case notifications
    }
}
